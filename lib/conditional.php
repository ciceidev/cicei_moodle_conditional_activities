<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

function evaluate_novelty($userid, $courseid, $cmid) {
    return !record_exists('log', 'userid', $userid, 'course', $courseid, 'cmid', $cmid);
}

/*function conditional_user_sections_visibility($courseid, $userid=0) {
}*/

function conditional_user_section_visible($sectionid, $userid=0) {

    global $USER;

    if (empty($userid)) {
        $userid = $USER->id;
    }

}

function conditional_user_course_visibility($courseid, $userid=0) {

    global $USER;

    if (empty($userid)) {
        $userid = $USER->id;
    }

    $activities = get_array_of_activities($courseid);
    $conditionalvisibilities = array();
    foreach ($activities as $activity) {
        $conditionalvisibilities[$activity->cm] = conditional_user_module_visible($activity->cm, $userid);
    }
    return $conditionalvisibilities;

}

function conditional_user_module_visible($modid, $userid=0) {

    global $USER, $CFG;

    if (empty($userid)) {
        $userid = $USER->id;
    }

    $coursemodule = get_record('course_modules', 'id', $modid);

    $course = get_record('course', 'id', $coursemodule->course);
    if (!$course->useconditionals) {
        return $coursemodule->visible;
    }

    if (!$coursemodule->visible) {
        return false;
    }

    $context = get_context_instance(CONTEXT_COURSE, $coursemodule->course);
    if ( has_capability('moodle/course:viewhiddenactivities', $context) ) {
        return true;
    }

    $conditional = get_record('conditional', 'moduleid', $modid);

    if( $conditional ) {
        if( $conditional->details && $conditional->inuse ) {
            $members = get_records('conditional_member', 'conditionalid', $conditional->id, 'id desc');
            if( count($members) == 0 ) {
                return true;
            }
        } else {
            return true;
        }
    } else {
        return true;
    }

    $expression = str_replace('not', '!', $conditional->details);

    foreach($members as $member) {

        if(strstr($expression, $member->tableid)) {

            switch($member->type) {

                case 'assignment':
                    if (!$instance = get_record($member->type, 'id', $member->instance)) {
                        $expression = str_replace($member->tableid, 'false', $expression);
                    } else {
                        if ($member->subtype == 'submit') {
                            $submission = count_records('assignment_submissions', 'assignment', $instance->id, 'userid', $userid);
                            if( eval('return '.$submission.' '.$member->op.' '.$member->value.' ;') ) {
                                $expression = str_replace($member->tableid, 'true', $expression);
                            } else {
                                $expression = str_replace($member->tableid, 'false', $expression);
                            }
                        } elseif ($member->subtype == 'grade') {
                            if($instance->grade > 0) {
                                require_once("$CFG->dirroot/mod/assignment/lib.php");
                                $grades = assignment_get_user_grades($instance, $userid);
                                if ($grades) {
                                    $grade = array_pop($grades);
                                    if (isset($grade->rawgrade)) {
                                        if( eval('return '.round($grade->rawgrade).' '.$member->op.' '.$member->value.' ;') ) {
                                            $expression = str_replace($member->tableid, 'true', $expression);
                                        } else {
                                            $expression = str_replace($member->tableid, 'false', $expression);
                                        }
                                    } else {
                                        $expression = str_replace($member->tableid, 'false', $expression);
                                    }
                                } else {
                                    $expression = str_replace($member->tableid, 'false', $expression);
                                }
                            } elseif($instance->grade < 0) {
                                require_once("$CFG->dirroot/mod/assignment/lib.php");
                                $grades = assignment_get_user_grades($instance, $userid);
                                if ($grades) {
                                    $grade = array_pop($grades);
                                    if (isset($grade->rawgrade)) {
                                        if( eval('return '.round($grade->rawgrade).' '.$member->op.' '.$member->value.' ;') ) {
                                            $expression = str_replace($member->tableid, 'true', $expression);
                                        } else {
                                            $expression = str_replace($member->tableid, 'false', $expression);
                                        }
                                    } else {
                                        $expression = str_replace($member->tableid, 'false', $expression);
                                    }
                                } else {
                                    $expression = str_replace($member->tableid, 'false', $expression);
                                }
                            } else {
                                $expression = str_replace($member->tableid, 'false', $expression);
                            }
                        }
                    }
                    break;
                case 'book':
                    $togetmoduleid = get_record('modules', 'name', 'book');
                    if ($module = get_record('course_modules', 'module', $togetmoduleid->id, 'instance', $member->instance)) {
                        $select = "userid = $userid AND module = 'book' AND action = 'view' AND cmid = $module->id";
                        $accessnum = count_records_select('log', $select);
                        if( eval('return '.$accessnum.' '.$member->op.' '.$member->value.' ;') ) {
                            $expression = str_replace($member->tableid, 'true', $expression);
                        } else {
                            $expression = str_replace($member->tableid, 'false', $expression);
                        }
                    } else {
                        $expression = str_replace($member->tableid, 'false', $expression);
                    }
                    break;
                case 'chat':
                    $participationsnum = count_records('chat_messages', 'chatid', $member->instance, 'userid', $userid);
                    if($member->subtype == 'say') {
                        if( eval('return '.$participationsnum.' > 0 ;') ) {
                            $expression = str_replace($member->tableid, 'true', $expression);
                        } else {
                            $expression = str_replace($member->tableid, 'false', $expression);
                        }
                    } elseif($member->subtype == 'notsay') {
                        if( eval('return '.$participationsnum.' == 0 ;') ) {
                            $expression = str_replace($member->tableid, 'true', $expression);
                        } else {
                            $expression = str_replace($member->tableid, 'false', $expression);
                        }
                    }
                    break;
                case 'choice':
                    if ($answer = get_record('choice_answers', 'choiceid', $member->instance, 'userid', $userid)) {
                        if ($member->subtype == 'anyanswer') {
                            $expression = str_replace($member->tableid, 'true', $expression);
                        } elseif ($member->subtype == 'particularanswer') {
                            if( eval('return '.$answer->optionid.' '.$member->op.' '.$member->value.' ;') ) {
                                $expression = str_replace($member->tableid, 'true', $expression);
                            } else {
                                $expression = str_replace($member->tableid, 'false', $expression);
                            }
                        }
                    } else {
                        $expression = str_replace($member->tableid, 'false', $expression);
                    }
                    break;
                case 'conditionalbook':
                    $togetmoduleid = get_record('modules', 'name', 'conditionalbook');
                    if ($module = get_record('course_modules', 'module', $togetmoduleid->id, 'instance', $member->instance)) {
                        $select = "userid = $userid AND module = 'conditionalbook' AND action = 'view' AND cmid = $module->id";
                        $accessnum = count_records_select('log', $select);
                        if( eval('return '.$accessnum.' '.$member->op.' '.$member->value.' ;') ) {
                            $expression = str_replace($member->tableid, 'true', $expression);
                        } else {
                            $expression = str_replace($member->tableid, 'false', $expression);
                        }
                    } else {
                        $expression = str_replace($member->tableid, 'false', $expression);
                    }
                    break;
                case 'dialogue':
                    $conversationsnum = count_records('dialogue_conversations', 'dialogueid', $member->instance, 'userid', $userid);
                    if( eval('return '.$conversationsnum.' '.$member->op.' '.$member->value.' ;') ) {
                        $expression = str_replace($member->tableid, 'true', $expression);
                    } else {
                        $expression = str_replace($member->tableid, 'false', $expression);
                    }
                    break;
                case 'forum':
                    if ($member->subtype == 'discussions') {
                        $participationsnum = count_records('forum_discussions', 'forum', $member->instance, 'userid', $userid);
                        if( eval('return '.$participationsnum.' '.$member->op.' '.$member->value.' ;') ) {
                            $expression = str_replace($member->tableid, 'true', $expression);
                        } else {
                            $expression = str_replace($member->tableid, 'false', $expression);
                        }
                    } elseif ($member->subtype == 'messages') {
                        $discussions = get_records('forum_discussions', 'forum', $member->instance, 'id asc');
                        $participationsnum = 0;
                        foreach ($discussions as $discussion) {
                            $participationsnum += count_records('forum_posts', 'discussion', $discussion->id, 'userid', $userid);
                        }
                        if( eval('return '.$participationsnum.' '.$member->op.' '.$member->value.' ;') ) {
                            $expression = str_replace($member->tableid, 'true', $expression);
                        } else {
                            $expression = str_replace($member->tableid, 'false', $expression);
                        }
                    } elseif( $member->subtype == 'success') {
                        $discussions = get_records_select('forum_discussions', "forum = $member->instance and userid = $userid", 'id  asc');
                        $responsesnum = 0;
                        foreach ($discussions as $discussion) {
                            $responsesnum += (count_records('forum_posts', 'discussion', $discussion->id) - 1);
                        }
                        if( eval('return '.$responsesnum.' '.$member->op.' '.$member->value.' ;') ) {
                            $expression = str_replace($member->tableid, 'true', $expression);
                        } else {
                            $expression = str_replace($member->tableid, 'false', $expression);
                        }
                    } elseif( $member->subtype == 'answers') {
                        $discussions = get_records('forum_discussions', 'forum', $member->instance, 'id asc');
                        $participationsnum = 0;
                        foreach ($discussions as $discussion) {
                            if ( ($discussion->userid == $userid) || record_exists('forum_posts', 'discussion', $discussion->id, 'userid', $userid) ) {
                                $participationsnum++;
                            }
                        }
                        if( eval('return '.$participationsnum.' '.$member->op.' '.$member->value.' ;') ) {
                            $expression = str_replace($member->tableid, 'true', $expression);
                        } else {
                            $expression = str_replace($member->tableid, 'false', $expression);
                        }
                    } elseif( $member->subtype == 'grade' ) {
                        if (!$instance = get_record($member->type, 'id', $member->instance)) {
                            $expression = str_replace($member->tableid, 'false', $expression);
                        }
                        if($instance->scale > 0) {
                            require_once("$CFG->dirroot/mod/forum/lib.php");
                            $grades = forum_get_user_grades($instance, $userid);
                            if ($grades) {
                                $grade = array_pop($grades);
                                if (isset($grade->rawgrade)) {
                                    if( eval('return '.round($grade->rawgrade).' '.$member->op.' '.$member->value.' ;') ) {
                                        $expression = str_replace($member->tableid, 'true', $expression);
                                    } else {
                                        $expression = str_replace($member->tableid, 'false', $expression);
                                    }
                                } else {
                                    $expression = str_replace($member->tableid, 'false', $expression);
                                }
                            } else {
                                $expression = str_replace($member->tableid, 'false', $expression);
                            }
                        } elseif($instance->scale < 0) {
                            require_once("$CFG->dirroot/mod/forum/lib.php");
                            $grades = forum_get_user_grades($instance, $userid);
                            if ($grades) {
                                $grade = array_pop($grades);
                                if (isset($grade->rawgrade)) {
                                    if( eval('return '.round($grade->rawgrade).' '.$member->op.' '.$member->value.' ;') ) {
                                        $expression = str_replace($member->tableid, 'true', $expression);
                                    } else {
                                        $expression = str_replace($member->tableid, 'false', $expression);
                                    }
                                } else {
                                    $expression = str_replace($member->tableid, 'false', $expression);
                                }
                            } else {
                                $expression = str_replace($member->tableid, 'false', $expression);
                            }
                        } else {
                            $expression = str_replace($member->tableid, 'false', $expression);
                        }
                    }
                    break;
                case 'glossary':
                    if( $member->subtype == 'entries') {
                        $entriesnum = count_records('glossary_entries', 'glossaryid', $member->instance, 'userid', $userid);
                        if( eval('return '.$entriesnum.' '.$member->op.' '.$member->value.' ;') ) {
                            $expression = str_replace($member->tableid, 'true', $expression);
                        } else {
                            $expression = str_replace($member->tableid, 'false', $expression);
                        }
                    } elseif( $member->subtype == 'comments') {
                        $entries = get_records('glossary_entries', 'glossaryid', $member->instance, 'id asc');
                        $commentsnum = 0;
                        foreach ($entries as $entry) {
                            $commentsnum += count_records('glossary_comments', 'entryid', $entry->id, 'userid', $userid);
                        }
                        if( eval('return '.$commentsnum.' '.$member->op.' '.$member->value.' ;') ) {
                            $expression = str_replace($member->tableid, 'true', $expression);
                        } else {
                            $expression = str_replace($member->tableid, 'false', $expression);
                        }
                    }
                    break;
                case 'lesson':
                    if( $member->subtype == 'grade') {
                        $lesson = get_record('lesson', 'id', $member->instance);
                        require_once("$CFG->dirroot/mod/lesson/lib.php");
                        if ($grades = lesson_get_user_grades($lesson, $userid)) {
                           $grade = array_pop($grades);
                           if (isset($grade->rawgrade)) {
                                if( eval('return '.$grade->rawgrade.' '.$member->op.' '.$member->value.' ;') ) {
                                    $expression = str_replace($member->tableid, 'true', $expression);
                                } else {
                                    $expression = str_replace($member->tableid, 'false', $expression);
                                }
                            } else {
                                $expression = str_replace($member->tableid, 'false', $expression);
                            }

                        } else {
                            $expression = str_replace($member->tableid, 'false', $expression);
                        }
                    } else {
                        $page = get_record('lesson_pages', 'id', $member->subtype);
                        $attempt = get_record('lesson_attempts', 'lessonid', $member->instance, 'pageid', $member->subtype, 'userid', $userid);
                        switch($page->qtype) {
                            case 1: /// short answer
                            case 2: /// true/false
                            case 3: /// multichoice
                                if (!isset($attempt->answerid)) {
                                    $expression = str_replace($member->tableid, 'false', $expression);
                                } else {
                                    if( eval('return '.$attempt->answerid.' '.$member->op.' '.$member->value.' ;') ) {
                                        $expression = str_replace($member->tableid, 'true', $expression);
                                    } else {
                                        $expression = str_replace($member->tableid, 'false', $expression);
                                    }
                                }
                                break;
                            case 8: /// numerical
                                if (!isset($attempt->useranswer)) {
                                    $expression = str_replace($member->tableid, 'false', $expression);
                                } else {
                                    if( eval('return '.$attempt->useranswer.' '.$member->op.' '.$member->value.' ;') ) {
                                        $expression = str_replace($member->tableid, 'true', $expression);
                                    } else {
                                        $expression = str_replace($member->tableid, 'false', $expression);
                                    }
                                }
                                break;
                            case 5: /// match
                                $useranswers = explode(',', $attempt->useranswer);
                                $answers = get_records_select('lesson_answers', "pageid = $member->subtype and response <> ''", 'id asc');
                                $j = 0;
                                $acertsnum = 0;
                                foreach ($answers as $answer) {
                                    if ($answer->id == $useranswers[$j]) {
                                        $acertsnum++;
                                    }
                                    $j++;
                                }
                                if( eval('return '.$acertsnum.' '.$member->op.' '.$member->value.' ;') ) {
                                    $expression = str_replace($member->tableid, 'true', $expression);
                                } else {
                                    $expression = str_replace($member->tableid, 'false', $expression);
                                }
                                break;
                            case 10: /// essay
                                if (!isset($attempt->useranswer)) {
                                    $expression = str_replace($member->tableid, 'false', $expression);
                                } else {
                                    $essayinfo = unserialize($attempt->useranswer);
                                    if( eval('return '.$essayinfo->score.' '.$member->op.' '.$member->value.' ;') ) {
                                        $expression = str_replace($member->tableid, 'true', $expression);
                                    } else {
                                        $expression = str_replace($member->tableid, 'false', $expression);
                                    }
                                }
                                break;
                            default:
                                $expression = str_replace($member->tableid, 'false', $expression);
                                break;
                        }
                    }
                    break;
                case 'quiz':
                    if( $member->subtype == 'grade') {
                        $quiz = get_record('quiz', 'id', $member->instance);
                        require_once("$CFG->dirroot/mod/quiz/lib.php");
                        if ($grades = quiz_get_user_grades($quiz, $userid)) {
                            $grade = array_pop($grades);
                            if (!isset($grade->rawgrade)) {
                                $expression = str_replace($member->tableid, 'false', $expression);
                            } else {
                                if( eval('return '.$grade->rawgrade.' '.$member->op.' '.($quiz->grade/100*$member->value).' ;') ) {
                                    $expression = str_replace($member->tableid, 'true', $expression);
                                } else {
                                    $expression = str_replace($member->tableid, 'false', $expression);
                                }
                            }
                        } else {
                            $expression = str_replace($member->tableid, 'false', $expression);
                        }
                    } else {
                        $questioninstance = get_record('quiz_question_instances', 'id', $member->subtype);
                        $question = get_record('question', 'id', $questioninstance->question);
                        $attempts = get_records_select('quiz_attempts', "quiz = $member->instance AND userid = $userid", 'attempt desc', '*', '0', '1');
                        if ($attempts) {
                            $attempt = current($attempts);
                            $neweststate = get_record('question_sessions', 'attemptid', $attempt->id, 'questionid', $question->id);
                            $state = get_record('question_states', 'id', $neweststate->newest);
                            if ($state->answer == '') {
                                $expression = str_replace($member->tableid, 'false', $expression);
                            } else {
                                switch($question->qtype) {
                                    case 'shortanswer':
                                        $quizanswer = get_record('question_answers', 'id', $member->value);
                                        $shortanswer = get_record('question_shortanswer', 'question', $question->id);
                                        if($shortanswer->usecase) {
                                            if( eval('return '.$state->answer.' '.$member->op.' '.$quizanswer->answer.' ;') ) {
                                                $expression = str_replace($member->tableid, 'true', $expression);
                                            } else {
                                                $expression = str_replace($member->tableid, 'false', $expression);
                                            }
                                        } else {
                                            if( eval('return '.strtolower($state->answer).' '.$member->op.' '.strtolower($quizanswer->answer).' ;') ) {
                                                $expression = str_replace($member->tableid, 'true', $expression);
                                            } else {
                                                $expression = str_replace($member->tableid, 'false', $expression);
                                            }
                                        }
                                    case 'truefalse':
                                        if( eval('return '.$state->answer.' '.$member->op.' '.$member->value.' ;') ) {
                                            $expression = str_replace($member->tableid, 'true', $expression);
                                        } else {
                                            $expression = str_replace($member->tableid, 'false', $expression);
                                        }
                                        break;
                                    case 'multichoice':
                                        $answer = explode(':', $state->answer);
                                        if($answer[1] == '') {
                                            $expression = str_replace($member->tableid, 'false', $expression);
                                        } else {
                                            $givenanswers = explode(',', $answer[1]);
                                            $multichoice = false;
                                            foreach ($givenanswers as $givenanswer) {
                                                if( (!$multichoice) && eval('return '.$givenanswer.' '.$member->op.' '.$member->value.' ;') ) {
                                                    $expression = str_replace($member->tableid, 'true', $expression);
                                                    $multichoice = true;
                                                }
                                            }
                                            if (!$multichoice) {
                                                $expression = str_replace($member->tableid, 'false', $expression);
                                            }
                                        }
                                        break;
                                    case 'numerical':
                                        if( eval('return '.$state->answer.' '.$member->op.' '.$member->value.' ;') ) {
                                            $expression = str_replace($member->tableid, 'true', $expression);
                                        } else {
                                            $expression = str_replace($member->tableid, 'false', $expression);
                                        }
                                        break;
                                    case 'match':
                                        $answers = explode(',', $state->answer);
                                        $acertsnum = 0;
                                        foreach ($answers as $eachanswer) {
                                            $answer = explode('-', $eachanswer);
                                            $questionmatchsub = get_record('question_match_sub', 'question', $question->id, 'code', $answer[1]);
                                            if($answer[0] == $questionmatchsub->id) {
                                                $acertsnum++;
                                            }
                                        }
                                        if( eval('return '.$acertsnum.' '.$member->op.' '.$member->value.' ;') ) {
                                            $expression = str_replace($member->tableid, 'true', $expression);
                                        } else {
                                            $expression = str_replace($member->tableid, 'false', $expression);
                                        }
                                        break;
                                    case 'calculated':
                                        $useranswers = explode('-', $state->answer);
                                        if($useranswers[1] == '') {
                                            $expression = str_replace($member->tableid, 'false', $expression);
                                        } else {
                                            $datasetnum = str_replace('dataset', '', $useranswers[0]);
                                            $datasetitems = get_records('question_dataset_items', 'itemnumber', $datasetnum, 'id asc');
                                            $answer = get_record('question_answers', 'question', $question->id);
                                            foreach ($datasetitems as $datasetitem) {
                                                $datasetdefinition = get_record('question_dataset_definitions', 'id', $datasetitem->definition);
                                                $answer->answer = str_replace('{'.$datasetdefinition->name.'}', $datasetitem->value, $answer->answer);
                                            }
                                            $result = eval('return '.$answer->answer.' ;');
                                            if( eval('return '.$useranswers[1].' '.$member->op.' '.$result.' ;') ) {
                                                $expression = str_replace($member->tableid, 'true', $expression);
                                            } else {
                                                $expression = str_replace($member->tableid, 'false', $expression);
                                            }
                                        }
                                        break;
                                    /*case 9: /// cloze
                                        break;*/
                                    case 'randomsamatch':
                                        $answers = explode(',', $state->answer);
                                        $acertsnum = 0;
                                        foreach ($answers as $eachanswer) {
                                            $answer = explode('-', $eachanswer);
                                            if(count_records('question_answers', 'id', $answer[1], 'question', $answer[0], 'fraction', 1)) {
                                                $acertsnum++;
                                            }
                                        }
                                        if( eval('return '.$acertsnum.' '.$member->op.' '.$member->value.' ;') ) {
                                            $expression = str_replace($member->tableid, 'true', $expression);
                                        } else {
                                            $expression = str_replace($member->tableid, 'false', $expression);
                                        }
                                        break;
                                    default:
                                        $expression = str_replace($member->tableid, 'false', $expression);
                                        break;
                                }
                            }

                        } else {
                            $expression = str_replace($member->tableid, 'false', $expression);
                        }
                    }
                    break;
                case 'resource':
                    $togetmoduleid = get_record('modules', 'name', 'resource');
                    $module = get_record('course_modules', 'module', $togetmoduleid->id, 'instance', $member->instance);
                    $select = "userid = $userid AND module = 'resource' AND action = 'view' AND cmid = $module->id";
                    $accessnum = count_records_select('log', $select);
                    if( eval('return '.$accessnum.' '.$member->op.' '.$member->value.' ;') ) {
                        $expression = str_replace($member->tableid, 'true', $expression);
                    } else {
                        $expression = str_replace($member->tableid, 'false', $expression);
                    }
                    break;
                case 'questionnaire':
                    if (record_exists('questionnaire_attempts', 'qid', $member->instance, 'userid', $userid)) {
                        $hasanswered = 1;
                    } else {
                        $hasanswered = 0;
                    }
                    if( eval('return '.$hasanswered.' '.$member->op.' '.$member->value.' ;') ) {
                        $expression = str_replace($member->tableid, 'true', $expression);
                    } else {
                        $expression = str_replace($member->tableid, 'false', $expression);
                    }
                    break;
                case 'lstest':
                    $lstest = get_record('lstest', 'id', $member->instance);
                    if ($member->subtype == 'access') {
                        $styles = get_records('lstest_styles', 'testsid', $lstest->testsid);
                        $hasaccess = false;
                        foreach ($styles as $style) {
                            if (record_exists('lstest_user_scores', 'userid', $userid, 'stylesid', $style->id)) {
                                $hasaccess = true;
                                break;
                            }
                        }
                        if( eval('return '.$hasaccess.' '.$member->op.' '.$member->value.' ;') ) {
                            $expression = str_replace($member->tableid, 'true', $expression);
                        } else {
                            $expression = str_replace($member->tableid, 'false', $expression);
                        }
                    } elseif ($member->subtype == 'style') {
                        $styles = get_records("lstest_styles", "testsid", $lstest->testsid, "id asc");
                        $levels = get_records("lstest_levels", "testsid", $lstest->testsid, "id asc");
                        $userlevels = array();
                        foreach ($styles as $style) {
                            if ($userscores = get_records_select("lstest_user_scores", "stylesid = '$style->id' AND userid = '$userid'", "time desc", "*", "0", "1")) {
                                $userscore = current($userscores);
                                $userlevels[$style->id] = $userscore->levelsid;
                            }
                        }
                        if (!empty($userlevels)) {
                            $result = array();
                            foreach ($styles as $style) {
                                $ispredominant = true;
                                foreach ($userlevels as $userlevel) {
                                    $ispredominant = ($userlevels[$style->id] >= $userlevel);
                                    if (!$ispredominant) {
                                        break;
                                    }
                                }
                                if ($ispredominant) {
                                    array_push($result, $style->id);
                                }
                            }
                        } else {
                            $result = false;
                        }
                        if (!empty($result)) {
                            foreach ($result as $style) {
                                if( eval('return '.$style.' '.$member->op.' '.$member->value.' ;') ) {
                                    $expression = str_replace($member->tableid, 'true', $expression);
                                }
                            }
                            $expression = str_replace($member->tableid, 'false', $expression);
                        } else {
                            $expression = str_replace($member->tableid, 'false', $expression);
                        }
                    }
                    break;
                case 'wiki':
                    $entries = get_records('wiki_entries', 'wikiid', $member->instance, 'id asc');
                    $participationsnum = 0;
                    foreach ($entries as $entry) {
                        $participationsnum += count_records('wiki_pages', 'wiki', $entry->id, 'userid', $userid);
                    }
                    if( eval('return '.$participationsnum.' '.$member->op.' '.$member->value.' ;') ) {
                        $expression = str_replace($member->tableid, 'true', $expression);
                    } else {
                        $expression = str_replace($member->tableid, 'false', $expression);
                    }
                    break;
                case 'workshop':
                    require_once("$CFG->dirroot/mod/workshop/lib.php");
                    $grades = workshop_grades($member->instance);
                    if (!isset($grades->grades[$userid])) {
                        $expression = str_replace($member->tableid, 'false', $expression);
                    } else {
                        if( eval('return '.$grades->grades[$userid].' '.$member->op.' '.$member->value.' ;') ) {
                            $expression = str_replace($member->tableid, 'true', $expression);
                        } else {
                            $expression = str_replace($member->tableid, 'false', $expression);
                        }
                    }
                    break;
                case 'role':
                    $userinformation = get_user_roles($context, $userid);
                    if (!$userinformation) {
                        $expression = str_replace($member->tableid, 'false', $expression);
                    } else {
                        $shortname = $userinformation[key($userinformation)]->shortname;
                        $switchrole  = optional_param('switchrole',-1, PARAM_INT);
                        if ($switchrole <> -1) {
                            $roles = get_all_roles();
                            $shortname = $roles[$switchrole]->shortname;
                        }
                        if( eval('return "'.$shortname.'" '.$member->op.' "'.$member->value.'" ;') ) {
                            $expression = str_replace($member->tableid, 'true', $expression);
                        } else {
                            $expression = str_replace($member->tableid, 'false', $expression);
                        }
                    }
                    break;
                case 'lang':
                    $userinformation = get_record('user', 'id', $userid);
                    if (!$userinformation->lang) {
                        $expression = str_replace($member->tableid, 'false', $expression);
                    } else {
                        if( eval('return "'.$userinformation->lang.'" '.$member->op.' "'.$member->value.'" ;') ) {
                            $expression = str_replace($member->tableid, 'true', $expression);
                        } else {
                            $expression = str_replace($member->tableid, 'false', $expression);
                        }
                    }
                    break;
                case 'country':
                    $userinformation = get_record('user', 'id', $userid);
                    if (!$userinformation->country) {
                        $expression = str_replace($member->tableid, 'false', $expression);
                    } else {
                        if( eval('return "'.$userinformation->country.'" '.$member->op.' "'.$member->value.'" ;') ) {
                            $expression = str_replace($member->tableid, 'true', $expression);
                        } else {
                            $expression = str_replace($member->tableid, 'false', $expression);
                        }
                    }
                    break;
                case 'picture':
                    $userinformation = get_record('user', 'id', $userid);
                    if (isset($userinformation->picture)) {
                        if( eval('return "'.$userinformation->picture.'" '.$member->op.' "'.$member->value.'" ;') ) {
                            $expression = str_replace($member->tableid, 'true', $expression);
                        } else {
                            $expression = str_replace($member->tableid, 'false', $expression);
                        }
                    } else {
                        $expression = str_replace($member->tableid, 'false', $expression);
                    }
                    break;
                case 'enrolment':
                    if ($userlogs = get_records_select('log', "userid= $userid and course= $course->id", 'time ASC', 'id,time', 0, 1)) {
                        $firstlog = current($userlogs);
                        $enrolmentdate = (time() - $firstlog->time);
                        if( ($enrolmentdate >= 0) && eval('return "'.$enrolmentdate.'" '.$member->op.' "'.($member->value*86400).'" ;') ) {
                            $expression = str_replace($member->tableid, 'true', $expression);
                        } else {
                            $expression = str_replace($member->tableid, 'false', $expression);
                        }
                    } else {
                        $expression = str_replace($member->tableid, 'false', $expression);
                    }
                    break;
                case 'startdate':
                    $timedifference = (time() - $course->startdate);
                    if( ($timedifference >= 0) && eval('return "'.$timedifference.'" '.$member->op.' "'.($member->value*86400).'" ;') ) {
                        $expression = str_replace($member->tableid, 'true', $expression);
                    } else {
                        $expression = str_replace($member->tableid, 'false', $expression);
                    }
                    break;
                case 'date':
                    $date = time() - $member->instance;
                    if( ($date >= 0) && eval('return "'.$date.'" '.$member->op.' "'.($member->value*86400).'" ;') ) {
                        $expression = str_replace($member->tableid, 'true', $expression);
                    } else {
                        $expression = str_replace($member->tableid, 'false', $expression);
                    }
                    break;
                case 'message':
                    $messagesnum = count_records('message', 'useridfrom', $userid);
                    if( eval('return '.$messagesnum.' '.$member->op.' '.$member->value.' ;') ) {
                        $expression = str_replace($member->tableid, 'true', $expression);
                    } else {
                        $expression = str_replace($member->tableid, 'false', $expression);
                    }
                    break;
                case 'subcourse':
                    if (!$instance = get_record($member->type, 'id', $member->instance)) {
                        $expression = str_replace($member->tableid, 'false', $expression);
                    } else {
                        require_once("$CFG->dirroot/mod/subcourse/lib.php");
                        $grades = subcourse_grades($instance->id);
                        if (isset($grades->grades[$userid]->rawgrade)) {
                            $grade=round($grades->grades[$userid]->rawgrade);
                            if( eval('return '.$grade.' '.$member->op.' '.$member->value.' ;') ) {
                                $expression = str_replace($member->tableid, 'true', $expression);
                            } else {
                                $expression = str_replace($member->tableid, 'false', $expression);
                            }
                        } else {
                            $expression = str_replace($member->tableid, 'false', $expression);
                        }
                    }
                    break;
                default:
                    $expression = str_replace($member->tableid, 'false', $expression);
                    break;
            }
        }
    }

    $conditionalresult = eval('return '.$expression.';');
    return $conditionalresult;

}

?>
