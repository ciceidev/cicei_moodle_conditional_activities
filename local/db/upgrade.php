<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

function xmldb_local_upgrade($oldversion=0) {
    //global $CFG, $db;
 
    $result = true;

    if ($result && $oldversion < 2009032400) {
        $result = $result && install_from_xmldb_file(dirname(__FILE__).'/install.xml');

        /// Define field info to be added to course_modules
        $table = new XMLDBTable('course_modules');
        $field = new XMLDBField('info');
        $field->setAttributes(XMLDB_TYPE_TEXT, 'medium', null, null, null, null, null, null, 'groupmembersonly');
        $result = $result && add_field($table, $field);
        unset($field);
        unset($table);
    
        /// Define field useconditionals to be added to course
        $table = new XMLDBTable('course');
        $field = new XMLDBField('useconditionals');
        $field->setAttributes(XMLDB_TYPE_INTEGER, '2', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, null, null, '0', 'defaultrole');
        $result = $result && add_field($table, $field);
        unset($field);
        unset($table);

        /// Define field uselabel to be added to course
        $table = new XMLDBTable('course');
        $field = new XMLDBField('uselabel');
        $field->setAttributes(XMLDB_TYPE_INTEGER, '2', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, null, null, '0', 'useconditionals');
        $result = $result && add_field($table, $field);
        unset($field);
        unset($table);

        /// Define field personalizedlabel to be added to course
        $table = new XMLDBTable('course');
        $field = new XMLDBField('personalizedlabel');
        $field->setAttributes(XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL, null, null, null, null, 'uselabel');
        $result = $result && add_field($table, $field);
        unset($field);
        unset($table);

    }

    if ($result && $oldversion < 2009090100) {

        /// Define field issection to be added to conditional
        $table = new XMLDBTable('conditional');
        $field = new XMLDBField('issection');
        $field->setAttributes(XMLDB_TYPE_INTEGER, '2', XMLDB_UNSIGNED, XMLDB_NOTNULL, null, null, null, '0', 'moduleid');

        /// Launch add field issection
        $result = $result && add_field($table, $field);

    }

    return $result;
}

?>
