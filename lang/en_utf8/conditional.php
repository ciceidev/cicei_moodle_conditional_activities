<?php
$string['activateconditional'] = 'Activate conditional';
$string['activities'] = 'Activities';
$string['activityinfo'] = 'Activity information';
$string['activityinfocreation'] = 'Creation of activity information for $a';
$string['activitylocks'] = 'Conditionals';
$string['addcondition'] = 'Add $a to grouping of members';
$string['agregateconditionswarning'] = 'You must fill at least one condition next in order to this activity will be locked.';
$string['agrupateconditions'] = 'Combine the different conditions using logic operators';
$string['assignmentgrade'] = 'mark obtained in the assignment';
$string['assignmentgradeselect'] = 'mark obtained in the assignment: ';
$string['assignmentparticulargrade'] = 'mark obtained in the assignment <B>$a</B> ';
$string['assignmentparticularhasntsubmit'] = 'The student has not sent the assignment <B>$a</B> ';
$string['assignmentparticularhassubmit'] = 'The student has sent the assignment <B>$a</B> ';
$string['assignmentstudentgrade'] = 'The mark obtained in <B>$a->instance</B> is $a->op ';
$string['assignmentstudenthasntsubmit'] = 'The student has not sent the assignment';
$string['assignmentstudenthassubmit'] = 'The student has sent the assignment';
$string['assignmentsubmit'] = 'assignment shipment';
$string['bookparticipanthasaccess'] = 'Student has access to book ';
$string['bookparticipanthasaccessdisplay'] = 'Student has access to book <B>$a</B> ';
$string['bookparticipanthasntaccess'] = 'Student has not access to book ';
$string['bookparticipanthasntaccessdisplay'] = 'Student has not access to book <B>$a</B> ';
$string['chatparticularhasntparticipated'] = 'The student has not participated in <B>$a</B> ';
$string['chatparticularhasparticipated'] = 'The student has participated in <B>$a</B> ';
$string['chatstudenthasntparticipated'] = 'The student has not participated in the chat';
$string['chatstudenthasparticipated'] = 'The student has participated in the chat';
$string['choiceanyanswer'] = 'The student has answered to the choice';
$string['choiceanyanswerdisplay'] = 'The student has answered to the choice <B>$a->instance</B>';
$string['choiceparticularanswer'] = 'Answer given to the choice';
$string['choiceparticularanswerdisplay'] = 'Answer given to <B>$a->instance</B> is $a->op $a->text';
$string['choiceparticularansweris'] = 'Answer given to the choice <B>$a</B> is ';
$string['choiceselect'] = 'Choose the choice: ';
$string['conditionalbookparticipanthasaccess'] = 'Student has access to conditional book ';
$string['conditionalbookparticipanthasaccessdisplay'] = 'Student has access to conditional book <B>$a</B> ';
$string['conditionalbookparticipanthasntaccess'] = 'Student has not access to conditional book ';
$string['conditionalbookparticipanthasntaccessdisplay'] = 'Student has not access to conditional book <B>$a</B> ';
$string['conditionaldisabled'] = '<b>Warning:</b> this conditional is deactivated and therefore it will not be used in the course. If you want to activate it click in the button \"Activate conditional\"';
$string['conditioncreation'] = 'Creation of conditions for $a';
$string['condition'] = 'Condition';
$string['conditions'] = 'Conditions';
$string['conditionscombination'] = 'Conditions combination: ';
$string['courseuselabel'] = 'To stand out new activities';
$string['createconditional'] = 'Save changes and create conditional';
$string['createconditionalandfinish'] = 'Create conditional and Finish buttons';
$string['createcondition'] = 'Create new condition';
$string['createdconditions'] = 'Created conditions for $a';
$string['date'] = 'Date';
$string['datetimedifference'] = 'Time passed (in days) since date ';
$string['datetimedifferencedisplay'] = 'Time passed since date $a->date is $a->op $a->days days';
$string['deactivateconditional'] = 'Deactivate conditional';
$string['delete'] = 'Delete';
$string['deleteusedcondition'] = 'An used condition is going to be deleted. So Conditions combination (at the bottom of this page) will be deleted and this activity will not be locked. Therefore you will have to fill Conditions combination again to turn this activity locked. Continue anyway?';
$string['dialogueconversations'] = 'Number of started dialogues';
$string['dialogueparticularconversations'] = 'Number of started dialogues in <B>$a</B> is ';
$string['dialogueparticipantconversations'] = 'Number of started dialogues in <B>$a->instance</B> is $a->op $a->value';
$string['distinct'] = 'different from';
$string['edit'] = 'Edit';
$string['editingactivityinfo'] = 'Editing activity info';
$string['editingconditionals'] = 'Editing conditionals';
$string['editingview'] = 'Editing course view';
$string['enrolment'] = 'Enrolment';
$string['enrolmenttimedifference'] = 'Time passed (in days) since student enrolment date in course is ';
$string['enrolmenttimedifferencedisplay'] = 'Time passed since student enrolment date in course is $a->op $a->days days';
$string['equal'] = 'equal to';
$string['errormarkst'] = 'There are no tests available!';
$string['forumanswers'] = 'Number of different discussions in which the student has posted';
$string['forumdiscussions'] = 'Discussions initiated by the student in the forum';
$string['forumgrade'] = 'Grade obtained by the student in the forum';
$string['forummessages'] = 'Messages of the student in the forum';
$string['forumconditionparticularanswers'] = 'The number of discussions been posted in <B>$a->instance</B> is $a->op $a->value';
$string['forumconditionparticulardiscussions'] = 'The number of discussions initiated in <B>$a->instance</B> is $a->op $a->value';
$string['forumconditionparticulargrade'] = 'The grade obtained in <B>$a->instance</B> is $a->op ';
$string['forumconditionparticularmessages'] = 'The number of messages placed in <B>$a->instance</B> is $a->op $a->value';
$string['forumconditionparticularsuccess'] = 'The number of answers obtained in <B>$a->instance</B> is $a->op $a->value';
$string['forumparticularanswers'] = 'The number of discussions been posted in the forum <B>$a</B> is: ';
$string['forumparticulardiscussions'] = 'The number of discussions initiated in the forum <B>$a</B> is: ';
$string['forumparticulargrade'] = 'The student grade obtained in the forum <B>$a</B> is: ';
$string['forumparticularmessages'] = 'The number of messages placed in the forum <B>$a</B> is: ';
$string['forumparticularsuccess'] = 'The number of answers obtained in the forum <B>$a</B> is: ';
$string['forumsuccess'] = 'Number of replies to student\'s messages';
$string['finish'] = 'Finish';
$string['general'] = 'General';
$string['glossarycomments'] = 'Commentaries added to the glossary by the student';
$string['glossaryconditionparticularcomments'] = 'The number of commentaries added to <B>$a->instance</B> is $a->op $a->value';
$string['glossaryconditionparticularentries'] = 'The number of entries added to <B>$a->instance</B> is $a->op $a->value';
$string['glossaryentries'] = 'Entries added to the glossary by the student';
$string['glossaryparticularcomments'] = 'The number of commentaries done to terms of the glossary <B>$a</B> is ';
$string['glossaryparticularentries'] = 'The number of entries added to the glossary <B>$a</B> is ';
$string['greater'] = 'greater than';
$string['greaterequal'] = 'greater or equal than';
$string['id'] = 'To use';
$string['invalidsyntax'] = '<b>There is an error:</b> probably the introduced syntax is not valid or any identifier number is not valid. You must check the conditions combination';
$string['less'] = 'less than';
$string['lessequal'] = 'less or equal than';
$string['lessonanswer'] = 'Answer given to a question of the lesson';
$string['lessonconditiongrade'] = 'The grade obtained in <B>$a->instance</B>, in percentage of the possible highest mark, is $a->op $a->value';
$string['lessonessaygrade'] = 'The score obtained in the question <B>$a</B> is ';
$string['lessongrade'] = 'Mark obtained in the lesson';
$string['lessonmatchs'] = 'The number of correct matches in the question <B>$a</B> is ';
$string['lessonparticulargrade'] = 'Grade obtained in the lesson <B>$a</B>, in percentage of the possible highest mark, is ';
$string['lessonquestionanswer'] = 'The answer given to the question <B>$a</B> is ';
$string['lessonquestionessay'] = 'The obtained score in the question <B>$a->question</B> of <B>$a->instance</B> is $a->op $a->value';
$string['lessonquestionmatch'] = 'The number of correct matches in the question <B>$a->question</B> of <B>$a->instance</B> is $a->op $a->value';
$string['lessonquestionnumerical'] = 'The answer given to the question <B>$a->question</B> of <B>$a->instance</B> is $a->op $a->value';
$string['lessonquestionshort'] = 'The answer given to the question <B>$a->question</B> of <B>$a->instance</B> is $a->op $a->answer';
$string['lessonselectquestion'] = 'Choose the due question of the lesson <B>$a</B>: ';
$string['locked'] = '(Locked) ';
$string['lstestaccessselecttest'] = 'Student has taken the test: ';
$string['lstestparticularstyle'] = 'Prevalent learning style in student according to <B>$a</B> test is ';
$string['lsteststudentaccess'] = 'Student has taken the test';
$string['lsteststudentstyle'] = 'Prevalent learning style in student';
$string['lsteststudenthasaccessdisplay'] = 'Student has taken <B>$a</B> test';
$string['lsteststudentstyledisplay'] = 'Prevalent learning style in student according to <B>$a->instance</B> test is $a->op $a->value';
$string['lsteststyleselecttest'] = 'Prevalent learning style in student according to test: ';
$string['message'] = 'Instant messages';
$string['messageconditiondisplay'] = 'The number of <B>instant messages</B> sent by participant is $a->op $a->value';
$string['new'] = 'NEW';
$string['newtitle'] = 'You have not seen this course element';
$string['nocreatedconditions'] = 'Conditions have not been created yet';
$string['notuseconditionals'] = 'Don\'t use Conditionals';
$string['personalized'] = 'Yes - personalized label';
$string['personalizedlabel'] = 'Personalized label';
$string['picture'] = 'Participant picture';
$string['picturehaschanged'] = 'Participant has changed his picture';
$string['picturehasnotchanged'] = 'Participant has not changed his picture';
$string['questionnairestudenthasanswer'] = 'Student has answered to questionnaire ';
$string['questionnairestudenthasntanswer'] = 'Student has not answered to questionnaire ';
$string['questionnairestudenthasanswerdisplay'] = 'Student has answered to questionnaire <B>$a</B>';
$string['questionnairestudenthasntanswerdisplay'] = 'Student has not answered to questionnaire <B>$a</B>';
$string['quizanswer'] = 'Answer given to a question of the quiz';
$string['quizconditiongrade'] = 'The mark obtained in <B>$a->instance</B>, in percentage of the possible highest mark, is $a->op $a->value';
$string['quizgrade'] = 'Mark obtained in the quiz';
$string['quizmatchs'] = 'The number of successes in the question <B>$a</B> is ';
$string['quizparticulargrade'] = 'The mark obtained in the quiz <B>$a</B>, in percentage of the possible highest mark, is ';
$string['quizquestionanswer'] = 'The answer given to the question <B>$a</B> is ';
$string['quizquestionmatch'] = 'The number of correct matches in the question <B>$a->question</B> of <B>$a->instance</B> is $a->op $a->value';
$string['quizquestionnumerical'] = 'The answer given to the question <B>$a->question</B> of <B>$a->instance</B> is $a->op $a->value';
$string['quizquestionrandom'] = 'The number of successes in the question <B>$a->question</B> of <B>$a->instance</B> is $a->op $a->value';
$string['quizquestionshort'] = 'The answer given to the question <B>$a->question</B> of <B>$a->instance</B> is $a->op $a->answer';
$string['quizselectquestion'] = 'Choose the wished question of the quiz <B>$a</B>: ';
$string['resourcestudenthasaccess'] = 'Student has access to resource ';
$string['resourcestudenthasaccessdisplay'] = 'Student has access to resource <B>$a</B> ';
$string['resourcestudenthasntaccess'] = 'Student has not access to resource ';
$string['resourcestudenthasntaccessdisplay'] = 'Student has not access to resource <B>$a</B> ';
$string['role'] = 'Participant role';
$string['roleparticular'] = 'The <B>role</B> of the user is $a->op $a->value';
$string['savecondition'] = 'Save condition';
$string['selectactivitytype'] = 'Choose type of item to apply condition to: ';
$string['sentmessage'] = 'The number of <B>instant messages</B> sent by participant is: ';
$string['startdate'] = 'Course start';
$string['starttimedifference'] = 'Time passed (in days) since course start date is ';
$string['starttimedifferencedisplay'] = 'Time passed since course start date is $a->op $a->days days';
$string['studentlang'] = 'Student language ';
$string['studentlangparticular'] = 'The <B>language</B> of the student is $a->op $a->value';
$string['studentcountry'] = 'Student country ';
$string['studentcountryparticular'] = 'The <B>country</B> of the student is $a->op $a->value';
$string['subcourseconditiongrade'] = 'The grade obtained in <B>$a->instance</B> is $a->op $a->value';
$string['subcoursegrade'] = 'Grade obtained in the subcourse ';
$string['subcourseparticulargrade'] = 'Grade obtained in the subcourse <B>$a</B> is ';
$string['useconditionals'] = 'Use conditionals';
$string['used'] = 'In use';
$string['userrole'] = 'The role of the user is ';
$string['wiki'] = 'The number of times that the student has published the wiki ';
$string['wikiis'] = ' is ';
$string['wikiparticular'] = 'The number of times that the student has published <B>$a->instance</B> is $a->op $a->value';
$string['workshop'] = 'The grade (in percentage of the possible highest mark) obtained in the workshop ';
$string['workshopis'] = ' is ';
$string['workshopparticular'] = 'The grade obtained in <B>$a->instance</B>, in percentage of the possible highest mark, is $a->op $a->value';
$string['yes'] = 'Yes - NEW label';
?>
