<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once("../../config.php");
require_once("$CFG->dirroot/course/lib.php");
require_once("conditionslib.php");

require_login();

$id = required_param('id', PARAM_INT);
$action = optional_param('action', NULL);
$conditionid = optional_param('conditionid', NULL, PARAM_INT);
$conditionalinstance = optional_param('instance', NULL, PARAM_INT);
$conditiontype = optional_param('conditiontype', NULL);
$subtype = optional_param('subtype', NULL);
$op = optional_param('op', NULL, PARAM_RAW);
$value = optional_param('value', NULL);
$day = optional_param('day', NULL);
$month = optional_param('month', NULL);
$year = optional_param('year', NULL);
// If noscript we must expect that params
$cancelconditioncreation = optional_param('cancelconditioncreation', NULL);

if (! $cm = get_record("course_modules", "id", $id)) {
    print_error('invalidcoursemodule');
}

if (! $course = get_record("course", "id", $cm->course)) {
    print_error('invalidcourse');
}

require_login($course);
$context = get_context_instance(CONTEXT_MODULE, $cm->id);
require_capability('moodle/local:createconditionals', $context);

if (! $module = get_record("modules", "id", $cm->module)) {
    print_error('cmunknown');
}

if (! $activity = get_record($module->name, "id", $cm->instance)) {
    print_error('cmunknown');
}

if(isset($conditiontype) and ($action=='cancel' || $cancelconditioncreation)) {
    redirect("conditions.php?id=$cm->id");
}

if(isset($conditiontype) and $action=='save') {
    conditional_save($cm->id, $conditiontype, $conditionalinstance, $subtype, $op, $value, $year, $month, $day, $conditionid);
    redirect("conditions.php?id=$cm->id");
}

$overridableroles = get_overridable_roles($context);
$assignableroles  = get_assignable_roles($context);
$currenttab = 'locks';
include_once($CFG->dirroot.'/'.$CFG->admin.'/roles/tabs.php');

print_heading_with_help(get_string('conditioncreation', 'conditional', $activity->name), 'conditionalcreation', 'conditional');

if (!$action) {
    conditional_define_condition($cm->id, $course->id, $conditionid);
} elseif($conditiontype and $action=='createstep1') {
    conditional_define_condition_step1($cm->id, $conditiontype, $course, $module, $conditionid);
} elseif($conditiontype and $action=='createstep2') {
    conditional_define_condition_step2($cm->id, $conditiontype, $course, $module, $conditionalinstance, $subtype, $op, $value, $conditionid);
} elseif($conditiontype and $action=='createstep3') {
    conditional_define_condition_step3($cm->id, $conditiontype, $conditionalinstance, $subtype, $op, $value, $conditionid);
}

print_footer($course);

?>