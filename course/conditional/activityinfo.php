<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once('../../config.php');
require_once("$CFG->dirroot/course/lib.php");

require_login();

$id = optional_param('id', 0, PARAM_INT);
$activityinfo = optional_param('activityinfo', NULL);
$createconditional = optional_param('createconditional', NULL);
$info = optional_param('info', NULL);

if (! $cm = get_record("course_modules", "id", $id)) {
    print_error('invalidcoursemodule');
}

if (! $course = get_record("course", "id", $cm->course)) {
    print_error('invalidcourse');
}

require_login($course);
$context = get_context_instance(CONTEXT_MODULE, $cm->id);
require_capability('moodle/local:assigncomments', $context);

if (! $module = get_record("modules", "id", $cm->module)) {
    print_error('cmunknown');
}

if (! $activity = get_record($module->name, "id", $cm->instance)) {
    print_error('cmunknown');
}

if(isset($activityinfo) || isset($createconditional)) {
    if(isset($info)) {
        $cm->info = $info;
        update_record('course_modules', $cm);
    }
    if (isset($activityinfo)) {
        redirect("$CFG->wwwroot/mod/$module->name/view.php?id=$cm->id", get_string("changessaved"), 1);
    } else if (isset($createconditional)) {
        redirect("conditions.php?id=$cm->id", get_string("changessaved"), 1);
    }
}

$overridableroles = get_overridable_roles($context);
$assignableroles  = get_assignable_roles($context);
$currenttab = 'info';
include_once($CFG->dirroot.'/'.$CFG->admin.'/roles/tabs.php');

print_heading_with_help(get_string('activityinfocreation', 'conditional', $activity->name), 'activityinfocreation', 'conditional');

echo "<form name='activityinfoform' method='POST' action='activityinfo.php?id=".$cm->id."'>";
echo '<center>';

//$usehtmleditor = can_use_html_editor();
//print_textarea($usehtmleditor, 10, 50, 660, 200, "info", $cm->info);
print_textarea(0, 10, 50, 660, 200, "info", $cm->info);
//if ($usehtmleditor) {
//    use_html_editor("info");
//}

echo '<br>';
echo '<br>';

echo '<input type="submit" name="activityinfo" value="'.get_string('savechanges').'">';
echo '<input type="submit" name="createconditional" value="'.get_string('createconditional', 'conditional').'">';

echo '</center>';
echo '</form>';
?>