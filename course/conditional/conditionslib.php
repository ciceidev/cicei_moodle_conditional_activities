<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

function conditional_delete_condition($cmid, $conditionid, $inuse) {
    delete_records('conditional_member', 'id', $conditionid);
    if ($inuse) {
        $conditional = get_record('conditional', 'moduleid', $cmid);
        $conditional->details = '';
        update_record('conditional', $conditional);
    }
}

function  conditional_activate($cmid) {
    $conditional = get_record('conditional', 'moduleid', $cmid);
    $conditional->inuse = true;
    update_record('conditional', $conditional);
}

function  conditional_deactivate($cmid) {
    $conditional = get_record('conditional', 'moduleid', $cmid);
    $conditional->inuse = false;
    update_record('conditional', $conditional);
}

function conditional_save($cmid, $conditiontype, $conditionalinstance=NULL, $subtype=NULL, $op=NULL, $value=NULL, $year=NULL, $month=NULL, $day=NULL, $conditionid=NULL) {
    if($conditional = get_record('conditional', 'moduleid', $cmid)) {
        $conditionalid = $conditional->id;
    } else {
        $data = new stdClass();
        $data->moduleid = $cmid;
        $conditionalid = insert_record('conditional', $data);
        unset($data);
    }

    $data = new stdClass();
    $data->conditionalid = $conditionalid;
    $data->type = $conditiontype;
    if(isset($subtype)) {
        $data->subtype = $subtype;
    }
    if ($conditiontype == 'date') {
        if (isset($day) && isset($month) && isset($year)) {
            $conditionalinstance = make_timestamp($year, $month, $day);
        }
    }
    if(isset($conditionalinstance)) {
        $data->instance = $conditionalinstance;
    }
    if(isset($op)) {
        $data->op = "$op";
    }
    if(isset($value)) {
        $data->value = $value;
    }
    if ($conditions = get_records('conditional_member', 'conditionalid', $conditionalid, 'tableid desc')) {
        $condition = current($conditions);
        $data->tableid = $condition->tableid + 1;
    } else {
        $data->tableid = 1;
    }
    if ($conditionid) {
        $data->id = $conditionid;
        $data->tableid = $conditions[$conditionid]->tableid;
        update_record('conditional_member', $data);
    } else {
        insert_record('conditional_member', $data);
    }
}

function conditional_agrupate($courseid, $cmid, $details='', &$agrupateconditionalerror, $return) {
    global $CFG;

    $conditional = get_record('conditional', 'moduleid', $cmid);
    $conditions = get_records('conditional_member', 'conditionalid', $conditional->id, 'tableid desc');
    $detailsprobe = $details;
    if ($details) {
        foreach ($conditions as $condition) {
            $detailsprobe = str_replace($condition->tableid, '', $detailsprobe);
        }
        $detailsprobe = str_replace('not', '', $detailsprobe);
        $detailsprobe = str_replace('and', '', $detailsprobe);
        $detailsprobe = str_replace('or', '', $detailsprobe);
        $detailsprobe = str_replace('(', '', $detailsprobe);
        $detailsprobe = str_replace(')', '', $detailsprobe);
    }
    if(str_replace(' ', '', $detailsprobe) != '') {
        $agrupateconditionalerror = true;
    } else {
        $detailsprobe = $details;
        foreach ($conditions as $condition) {
            $detailsprobe = str_replace($condition->tableid, 'true', $detailsprobe);
        }
        $detailsprobe = str_replace('not', '!', $detailsprobe);
        if( (str_replace(' ', '', $detailsprobe) == '') || eval('return (('.$detailsprobe.' == true) or ('.$detailsprobe.' == false));') == true) {
            $conditional->details = $details;
            update_record('conditional', $conditional);
            if ($return) {
                redirect("$CFG->wwwroot/course/view.php?id=$courseid", get_string("changessaved"), 1);
            } else {
                redirect("conditions.php?id=$cmid");
            }
        } else {
            $agrupateconditionalerror = true;
        }
    }
}

function conditional_define_condition($cmid, $courseid, $conditionid=NULL) {
    $modules = get_records_select('modules', "visible = 1 and (name = 'assignment' or name = 'chat' or name = 'choice' or name = 'forum' or name = 'glossary' or name = 'lesson' or name = 'quiz' or name = 'resource' or name = 'wiki' or name = 'workshop' or name = 'book' or name = 'conditionalbook' or name = 'dialogue' or name = 'lstest' or name = 'questionnaire' or name = 'subcourse')", 'id asc');

    foreach ($modules as $eachmodule) {
        if(count_records_select('course_modules', "id <> $cmid and course = $courseid and module = $eachmodule->id") > 0) {
            $options["$eachmodule->name"] = get_string("modulename", $eachmodule->name);
        }
    }

    $options['role'] = get_string('role', 'conditional');
    $options['lang'] = get_string('studentlang', 'conditional');
    $options['country'] = get_string('studentcountry', 'conditional');
    $options['picture'] = get_string('picture', 'conditional');
    $options['enrolment'] = get_string('enrolment', 'conditional');
    $options['startdate'] = get_string('startdate', 'conditional');
    $options['date'] = get_string('date', 'conditional');
    $options['message'] = get_string('message', 'conditional');

    echo '<center>';
    echo "<form name='createconditionform' method='POST' action='conditioncreation.php?id=".$cmid."'>";
    print_string('selectactivitytype', 'conditional');
    choose_from_menu($options, 'conditiontype', '', '');
    echo '<br><br>';
    echo '<input type="submit" name="createconditional" value="'.get_string('continue').'">';
    echo '<input type="hidden" name="action" value="createstep1">';
    echo '<input type="submit" name="cancelconditioncreation" value="'.get_string('cancel').'" onclick="document.createconditionform.action.value = \'cancel\';"/>';
    echo '<input type="hidden" name="conditionid" value="'.$conditionid.'">';
    echo '</form>';
    echo '</center>';
}

function conditional_define_condition_step1($cmid, $conditiontype, $course, $module, $conditionid=NULL) {
    global $CFG;

    echo "<form name='createconditionform' method='POST' action='conditioncreation.php?id=".$cmid."'>";
    echo '<center>';

    $conditionalmodule = get_record('modules', 'name', $conditiontype);

    switch($conditiontype) {

        case 'assignment':
            $options = array('submit'=>get_string('assignmentsubmit', 'conditional'), 'grade'=>get_string('assignmentgrade', 'conditional'));
            choose_from_menu($options, 'subtype', '', '');
            echo '<br><br>';
            echo '<input type="submit" name="createconditional2" value="'.get_string('continue').'">';
            echo '<input type="hidden" name="action" value="createstep2">';
            break;
        case 'book';
            $options = array('>'=>get_string('bookparticipanthasaccess', 'conditional'), '=='=>get_string('bookparticipanthasntaccess', 'conditional'));
            choose_from_menu($options, 'op', '', '');
            unset($options);
            $books = get_all_instances_in_course($conditiontype, $course);
            foreach ($books as $eachbook) {
                if ($conditionalmodule->id == $module->id) {
                    $cmconditional = get_record('course_modules', 'course', $course->id, 'module', $module->id, 'instance', $eachbook->id);
                    if ( $cmconditional->id != $cmid ) {
                        $options[$eachbook->id] = $eachbook->name;
                    }
                } else {
                    $options[$eachbook->id] = $eachbook->name;
                }
            }
            choose_from_menu($options, 'instance', '', '');
            echo '<br><br>';
            echo '<input type="hidden" name="value" value="0">';
            echo '<input type="hidden" name="subtype" value="access">';
            echo '<input type="submit" name="saveconditional" value="'.get_string('savecondition', 'conditional').'">';
            echo '<input type="hidden" name="action" value="save">';
            break;
        case 'chat':
            $options = array('say'=>get_string('chatstudenthasparticipated', 'conditional'), 'notsay'=>get_string('chatstudenthasntparticipated', 'conditional'));
            choose_from_menu($options, 'subtype', '', '');
            unset($options);
            $chats = get_all_instances_in_course($conditiontype, $course);
            foreach ($chats as $eachchat) {
                if ($conditionalmodule->id == $module->id) {
                    $cmconditional = get_record('course_modules', 'course', $course->id, 'module', $module->id, 'instance', $eachchat->id);
                    if ( $cmconditional->id != $cmid ) {
                        $options[$eachchat->id] = $eachchat->name;
                    }
                } else {
                    $options[$eachchat->id] = $eachchat->name;
                }
            }
            choose_from_menu($options, 'instance', '', '');
            echo '<br><br>';
            echo '<input type="submit" name="saveconditional" value="'.get_string('savecondition', 'conditional').'">';
            echo '<input type="hidden" name="action" value="save">';
            break;
        case 'choice':
            $options = array('anyanswer'=>get_string('choiceanyanswer', 'conditional'), 'particularanswer'=>get_string('choiceparticularanswer', 'conditional'));
            choose_from_menu($options, 'subtype', '', '');
            echo '<br><br>';
            echo '<input type="submit" name="createconditional2" value="'.get_string('continue').'">';
            echo '<input type="hidden" name="action" value="createstep2">';
            break;
        case 'conditionalbook';
            $options = array('>'=>get_string('conditionalbookparticipanthasaccess', 'conditional'), '=='=>get_string('conditionalbookparticipanthasntaccess', 'conditional'));
            choose_from_menu($options, 'op', '', '');
            unset($options);
            $conditionalbooks = get_all_instances_in_course($conditiontype, $course);
            foreach ($conditionalbooks as $eachconditionalbook) {
                if ($conditionalmodule->id == $module->id) {
                    $cmconditional = get_record('course_modules', 'course', $course->id, 'module', $module->id, 'instance', $eachconditionalbook->id);
                    if ( $cmconditional->id != $cmid ) {
                        $options[$eachconditionalbook->id] = $eachconditionalbook->name;
                    }
                } else {
                    $options[$eachconditionalbook->id] = $eachconditionalbook->name;
                }
            }
            choose_from_menu($options, 'instance', '', '');
            echo '<br><br>';
            echo '<input type="hidden" name="value" value="0">';
            echo '<input type="hidden" name="subtype" value="access">';
            echo '<input type="submit" name="saveconditional" value="'.get_string('savecondition', 'conditional').'">';
            echo '<input type="hidden" name="action" value="save">';
            break;
        case 'dialogue':
            $options = array('conversations'=>get_string('dialogueconversations', 'conditional'));
            choose_from_menu($options, 'subtype', '', '');
            unset($options);
            $dialogues = get_all_instances_in_course($conditiontype, $course);
            foreach ($dialogues as $eachdialogue) {
                if ($conditionalmodule->id == $module->id) {
                    $cmconditional = get_record('course_modules', 'course', $course->id, 'module', $module->id, 'instance', $eachdialogue->id);
                    if ( $cmconditional->id != $cmid ) {
                        $options[$eachdialogue->id] = $eachdialogue->name;
                    }
                } else {
                    $options[$eachdialogue->id] = $eachdialogue->name;
                }
            }
            choose_from_menu($options, 'instance', '', '');
            echo '<br><br>';
            echo '<input type="submit" name="createconditional2" value="'.get_string('continue').'">';
            echo '<input type="hidden" name="action" value="createstep2">';
            break;
        case 'forum':
            $forums = get_all_instances_in_course($conditiontype, $course);
            foreach ($forums as $eachforum) {
                if ($conditionalmodule->id == $module->id) {
                    $cmconditional = get_record('course_modules', 'course', $course->id, 'module', $module->id, 'instance', $eachforum->id);
                    if ( $cmconditional->id != $cmid ) {
                        $options[$eachforum->id] = $eachforum->name;
                    }
                } else {
                    $options[$eachforum->id] = $eachforum->name;
                }
            }
            choose_from_menu($options, 'instance', '', '');
            echo '<br><br>';
            echo '<input type="submit" name="createconditional2" value="'.get_string('continue').'">';
            echo '<input type="hidden" name="action" value="createstep2">';
            break;
        case 'glossary':
            $options = array('entries'=>get_string('glossaryentries', 'conditional'), 'comments'=>get_string('glossarycomments', 'conditional'));
            choose_from_menu($options, 'subtype', '', '');
            unset($options);
            $glossarys = get_all_instances_in_course($conditiontype, $course);
            foreach ($glossarys as $eachglossary) {
                if ($conditionalmodule->id == $module->id) {
                    $cmconditional = get_record('course_modules', 'course', $course->id, 'module', $module->id, 'instance', $eachglossary->id);
                    if ( $cmconditional->id != $cmid ) {
                        $options[$eachglossary->id] = $eachglossary->name;
                    }
                } else {
                    $options[$eachglossary->id] = $eachglossary->name;
                }
            }
            choose_from_menu($options, 'instance', '', '');
            echo '<br><br>';
            echo '<input type="submit" name="createconditional2" value="'.get_string('continue').'">';
            echo '<input type="hidden" name="action" value="createstep2">';
            break;
        case 'lesson':
            $options = array('grade'=>get_string('lessongrade', 'conditional'), 'answer'=>get_string('lessonanswer', 'conditional'));
            choose_from_menu($options, 'subtype', '', '');
            unset($options);
            $lessons = get_all_instances_in_course($conditiontype, $course);
            foreach ($lessons as $eachlesson) {
                if ($conditionalmodule->id == $module->id) {
                    $cmconditional = get_record('course_modules', 'course', $course->id, 'module', $module->id, 'instance', $eachlesson->id);
                    if ( $cmconditional->id != $cmid ) {
                        $options[$eachlesson->id] = $eachlesson->name;
                    }
                } else {
                    $options[$eachlesson->id] = $eachlesson->name;
                }
            }
            choose_from_menu($options, 'instance', '', '');
            echo '<br><br>';
            echo '<input type="submit" name="createconditional2" value="'.get_string('continue').'">';
            echo '<input type="hidden" name="action" value="createstep2">';
            break;
        case 'quiz':
            $options = array('grade'=>get_string('quizgrade', 'conditional'), 'answer'=>get_string('quizanswer', 'conditional'));
            choose_from_menu($options, 'subtype', '', '');
            unset($options);
            $quizs = get_all_instances_in_course($conditiontype, $course);
            foreach ($quizs as $eachquiz) {
                if ($conditionalmodule->id == $module->id) {
                    $cmconditional = get_record('course_modules', 'course', $course->id, 'module', $module->id, 'instance', $eachquiz->id);
                    if ( $cmconditional->id != $cmid ) {
                        $options[$eachquiz->id] = $eachquiz->name;
                    }
                } else {
                    $options[$eachquiz->id] = $eachquiz->name;
                }
            }
            choose_from_menu($options, 'instance', '', '');
            echo '<br><br>';
            echo '<input type="submit" name="createconditional2" value="'.get_string('continue').'">';
            echo '<input type="hidden" name="action" value="createstep2">';
            break;
        case 'resource';
            $options = array('>'=>get_string('resourcestudenthasaccess', 'conditional'), '=='=>get_string('resourcestudenthasntaccess', 'conditional'));
            choose_from_menu($options, 'op', '', '');
            unset($options);
            $resources = get_all_instances_in_course($conditiontype, $course);
            foreach ($resources as $eachresource) {
                if ($conditionalmodule->id == $module->id) {
                    $cmconditional = get_record('course_modules', 'course', $course->id, 'module', $module->id, 'instance', $eachresource->id);
                    if ( $cmconditional->id != $cmid ) {
                        $options[$eachresource->id] = $eachresource->name;
                    }
                } else {
                    $options[$eachresource->id] = $eachresource->name;
                }
            }
            choose_from_menu($options, 'instance', '', '');
            echo '<br><br>';
            echo '<input type="hidden" name="value" value="0">';
            echo '<input type="hidden" name="subtype" value="access">';
            echo '<input type="submit" name="saveconditional" value="'.get_string('savecondition', 'conditional').'">';
            echo '<input type="hidden" name="action" value="save">';
            break;
        case 'wiki':
            print_string('wiki', 'conditional');
            $wikis = get_all_instances_in_course($conditiontype, $course);
            foreach ($wikis as $eachwiki) {
                if ($conditionalmodule->id == $module->id) {
                    $cmconditional = get_record('course_modules', 'course', $course->id, 'module', $module->id, 'instance', $eachwiki->id);
                    if ( $cmconditional->id != $cmid ) {
                        $options[$eachwiki->id] = $eachwiki->name;
                    }
                } else {
                    $options[$eachwiki->id] = $eachwiki->name;
                }
            }
            choose_from_menu($options, 'instance', '', '');
            print_string('wikiis', 'conditional');
            $options = array('>'=>'>', '<'=>'<', '<='=>'<=', '>='=>'>=');
            choose_from_menu($options, 'op', '', '');
            unset($options);
            for($i=0; $i<=100 ;$i++) {
                $options[$i] = $i;
            }
            choose_from_menu($options, 'value', '', '');
            echo '<br><br>';
            echo '<input type="submit" name="saveconditional" value="'.get_string('savecondition', 'conditional').'">';
            echo '<input type="hidden" name="action" value="save">';
            break;
        case 'questionnaire':
            $options = array('>'=>get_string('questionnairestudenthasanswer', 'conditional'), '=='=>get_string('questionnairestudenthasntanswer', 'conditional'));
            choose_from_menu($options, 'op', '', '');
            unset($options);
            $questionnaires = get_all_instances_in_course($conditiontype, $course);
            foreach ($questionnaires as $eachquestionnaire) {
                if ($conditionalmodule->id == $module->id) {
                    $cmconditional = get_record('course_modules', 'course', $course->id, 'module', $module->id, 'instance', $eachquestionnaire->id);
                    if ( $cmconditional->id != $cmid ) {
                        $options[$eachquestionnaire->id] = $eachquestionnaire->name;
                    }
                } else {
                    $options[$eachquestionnaire->id] = $eachquestionnaire->name;
                }
            }
            choose_from_menu($options, 'instance', '', '');
            echo '<br><br>';
            echo '<input type="hidden" name="value" value="0">';
            echo '<input type="hidden" name="subtype" value="answer">';
            echo '<input type="submit" name="saveconditional" value="'.get_string('savecondition', 'conditional').'">';
            echo '<input type="hidden" name="action" value="save">';
            break;
        case 'lstest':
            $options = array('access'=>get_string('lsteststudentaccess', 'conditional'), 'style'=>get_string('lsteststudentstyle', 'conditional'));
            choose_from_menu($options, 'subtype', '', '');
            echo '<br><br>';
            echo '<input type="submit" name="createconditional2" value="'.get_string('continue').'">';
            echo '<input type="hidden" name="action" value="createstep2">';
            break;
        case 'workshop':
            print_string('workshop', 'conditional');
            $workshops = get_all_instances_in_course($conditiontype, $course);
            foreach ($workshops as $eachworkshop) {
                if ($conditionalmodule->id == $module->id) {
                    $cmconditional = get_record('course_modules', 'course', $course->id, 'module', $module->id, 'instance', $eachworkshop->id);
                    if ( $cmconditional->id != $cmid ) {
                        $options[$eachworkshop->id] = $eachworkshop->name;
                    }
                } else {
                    $options[$eachworkshop->id] = $eachworkshop->name;
                }
            }
            choose_from_menu($options, 'instance', '', '');
            print_string('workshopis', 'conditional');
            $options = array('=='=>'=', '!='=>'<>', '>'=>'>', '<'=>'<', '<='=>'<=', '>='=>'>=');
            choose_from_menu($options, 'op', '', '');
            unset($options);
            for($i=0; $i<=100 ;$i++) {
                $options[$i] = $i.' %';
            }
            choose_from_menu($options, 'value', '', '');
            echo '<br><br>';
            echo '<input type="submit" name="saveconditional" value="'.get_string('savecondition', 'conditional').'">';
            echo '<input type="hidden" name="action" value="save">';
            break;
        case 'role':
            $allroles = array();
            if ($roles = get_all_roles()) {
                foreach ($roles as $role) {
                    $rolename = strip_tags(format_string($role->name, true));
                    $allroles[$role->shortname] = $rolename;
                }
            }
            print_string('userrole', 'conditional');
            $options = array('=='=>'=', '!='=>'<>');
            choose_from_menu($options, 'op', '', '');
            choose_from_menu($allroles, 'value', '', '');
            echo '<br><br>';
            echo '<input type="submit" name="saveconditional" value="'.get_string('savecondition', 'conditional').'">';
            echo '<input type="hidden" name="action" value="save">';
            break;
        case 'lang':
            print_string('studentlang', 'conditional');
            $options = array('=='=>'=', '!='=>'<>');
            choose_from_menu($options, 'op', '', '');
            choose_from_menu(get_list_of_languages(), 'value', current_language(), '');
            echo '<br><br>';
            echo '<input type="submit" name="saveconditional" value="'.get_string('savecondition', 'conditional').'">';
            echo '<input type="hidden" name="action" value="save">';
            break;
        case 'country':
            print_string('studentcountry', 'conditional');
            $options = array('=='=>'=', '!='=>'<>');
            choose_from_menu($options, 'op', '', '');
            choose_from_menu(get_list_of_countries(), 'value', $CFG->country, '');
            echo '<br><br>';
            echo '<input type="submit" name="saveconditional" value="'.get_string('savecondition', 'conditional').'">';
            echo '<input type="hidden" name="action" value="save">';
            break;
        case 'picture':
            $options = array('>' => get_string('picturehaschanged', 'conditional'), '==' => get_string('picturehasnotchanged', 'conditional'));
            choose_from_menu($options, 'op', '', '');
            echo '<br><br>';
            echo '<input type="hidden" name="value" value="0">';
            echo '<input type="submit" name="saveconditional" value="'.get_string('savecondition', 'conditional').'">';
            echo '<input type="hidden" name="action" value="save">';
            break;
        case 'enrolment':
            print_string('enrolmenttimedifference', 'conditional');
            $options = array('>'=>'>', '<'=>'<');
            choose_from_menu($options, 'op', '', '');
            unset($options);
            for ($i=0; $i <= 1000; $i++) {
                $options[$i] = $i;
            }
            choose_from_menu($options, 'value', '', '');
            echo '<br><br>';
            echo '<input type="submit" name="saveconditional" value="'.get_string('savecondition', 'conditional').'">';
            echo '<input type="hidden" name="action" value="save">';
            break;
        case 'startdate':
            print_string('starttimedifference', 'conditional');
            $options = array('>'=>'>', '<'=>'<');
            choose_from_menu($options, 'op', '', '');
            unset($options);
            for ($i=0; $i <= 1000; $i++) {
                $options[$i] = $i;
            }
            choose_from_menu($options, 'value', '', '');
            echo '<br><br>';
            echo '<input type="submit" name="saveconditional" value="'.get_string('savecondition', 'conditional').'">';
            echo '<input type="hidden" name="action" value="save">';
            break;
        case 'date':
            print_string('datetimedifference', 'conditional');
            print_date_selector('day', 'month', 'year');
            print_string('wikiis', 'conditional');
            $options = array('>'=>'>', '<'=>'<');
            choose_from_menu($options, 'op', '', '');
            unset($options);
            for ($i=0; $i <= 1000; $i++) {
                $options[$i] = $i;
            }
            choose_from_menu($options, 'value', '', '');
            echo '<br><br>';
            echo '<input type="submit" name="saveconditional" value="'.get_string('savecondition', 'conditional').'">';
            echo '<input type="hidden" name="action" value="save">';
            break;
        case 'message':
            print_string('sentmessage', 'conditional');
            $options = array('>'=>'>', '<'=>'<', '<='=>'<=', '>='=>'>=');
            choose_from_menu($options, 'op', '>', '');
            unset($options);
            for ($i=0; $i <= 100; $i++) {
                $options[$i] = $i;
            }
            choose_from_menu($options, 'value', '0', '');
            echo '<br><br>';
            echo '<input type="submit" name="saveconditional" value="'.get_string('savecondition', 'conditional').'">';
            echo '<input type="hidden" name="action" value="save">';
            break;
        case 'subcourse':
            $subcourses = get_all_instances_in_course($conditiontype, $course);
            foreach ($subcourses as $eachsubcourse) {
                if ($conditionalmodule->id == $module->id) {
                    $cmconditional = get_record('course_modules', 'course', $course->id, 'module', $module->id, 'instance', $eachsubcourse->id);
                    if ( $cmconditional->id != $cmid ) {
                        $options[$eachsubcourse->id] = $eachsubcourse->name;
                    }
                } else {
                $options[$eachsubcourse->id] = $eachsubcourse->name;
                }
            }
            print_string('subcoursegrade', 'conditional');
            choose_from_menu($options, 'instance', '', '');
            echo '<br><br>';
            echo '<input type="submit" name="createconditional2" value="'.get_string('continue').'">';
            echo '<input type="hidden" name="action" value="createstep2">';
            break;
    }
    echo '<input type="hidden" name="conditiontype" value="'.$conditiontype.'">';
    echo '<input type="hidden" name="conditionid" value="'.$conditionid.'">';
    echo '<input type="submit" name="cancelconditioncreation" value="'.get_string('cancel').'" onclick="document.createconditionform.action.value = \'cancel\';"/>';
    echo '</center>';
    echo '</form>';
}

function conditional_define_condition_step2($cmid, $conditiontype, $course, $module, $conditionalinstance=NULL, $subtype=NULL, $op=NULL, $value=NULL, $conditionid=NULL) {
    echo "<form name='createconditionform' method='POST' action='conditioncreation.php?id=".$cmid."'>";
    echo '<center>';

    $conditionalmodule = get_record('modules', 'name', $conditiontype);

    switch($conditiontype) {
        case 'assignment':
            $assignments = get_all_instances_in_course($conditiontype, $course);
            if($subtype == 'submit') {
                $options = array('>'=>get_string('assignmentstudenthassubmit', 'conditional'), '=='=>get_string('assignmentstudenthasntsubmit', 'conditional'));
                choose_from_menu($options, 'op', '', '');

                unset($options);
                foreach ($assignments as $eachassignment) {
                    if ($conditionalmodule->id == $module->id) {
                        $cmconditional = get_record('course_modules', 'course', $course->id, 'module', $module->id, 'instance', $eachassignment->id);
                        if ( ($eachassignment->assignmenttype != 'offline') and ($cmconditional->id != $cmid) ) {
                            $options[$eachassignment->id] = $eachassignment->name;
                        }
                    } else {
                        if ( $eachassignment->assignmenttype != 'offline' ) {
                            $options[$eachassignment->id] = $eachassignment->name;
                        }
                    }
                }
                choose_from_menu($options, 'instance', '', '');

                echo '<input type="hidden" name="value" value="0">';
                echo '<br><br>';
                echo '<input type="submit" name="saveconditional" value="'.get_string('savecondition', 'conditional').'">';
                echo '<input type="hidden" name="action" value="save">';
            } elseif($subtype == 'grade') {
                print_string('assignmentgradeselect', 'conditional');

                foreach ($assignments as $eachassignment) {
                    if ($conditionalmodule->id == $module->id) {
                        $cmconditional = get_record('course_modules', 'course', $course->id, 'module', $module->id, 'instance', $eachassignment->id);
                        if ( ($eachassignment->grade != 0) and ($cmconditional->id != $cmid) ) {
                            $options[$eachassignment->id] = $eachassignment->name;
                        }
                    } else {
                        if ($eachassignment->grade != 0) {
                            $options[$eachassignment->id] = $eachassignment->name;
                        }
                    }
                }
                if (isset($options)) {
                    choose_from_menu($options, 'instance', '', '');
                } else {
                    choose_from_menu("", 'instance', '', '');
                }
                echo '<br><br>';
                echo '<input type="submit" name="createconditional3" value="'.get_string('continue').'">';
                echo '<input type="hidden" name="action" value="createstep3">';
            }
            echo '<input type="hidden" name="subtype" value="'.$subtype.'">';
            break;
        case 'choice':
            print_string('choiceselect', 'conditional');
            $choices = get_all_instances_in_course($conditiontype, $course);
            foreach ($choices as $eachchoice) {
                if ($conditionalmodule->id == $module->id) {
                    $cmconditional = get_record('course_modules', 'course', $course->id, 'module', $module->id, 'instance', $eachchoice->id);
                    if ( $cmconditional->id != $cmid ) {
                        $options[$eachchoice->id] = $eachchoice->name;
                    }
                } else {
                    $options[$eachchoice->id] = $eachchoice->name;
                }
            }
            choose_from_menu($options, 'instance', '', '');
            echo '<br><br>';
            echo '<input type="hidden" name="subtype" value="'.$subtype.'">';
            if ($subtype == 'anyanswer') {
                echo '<input type="submit" name="saveconditional" value="'.get_string('savecondition', 'conditional').'">';
                echo '<input type="hidden" name="action" value="save">';
            } elseif ($subtype == 'particularanswer') {
                echo '<input type="submit" name="createconditional3" value="'.get_string('continue').'">';
                echo '<input type="hidden" name="action" value="createstep3">';
            }
            break;
        case 'dialogue':
            $dialogue = get_record('dialogue', 'id', $conditionalinstance);
            print_string('dialogueparticularconversations', 'conditional', $dialogue->name);
            $options = array('>'=>'>', '<'=>'<', '<='=>'<=', '>='=>'>=');
            choose_from_menu($options, 'op', '>', '');
            unset($options);
            for($i=0; $i<=100 ;$i++) {
                $options[$i] = $i;
            }
            choose_from_menu($options, 'value', '0', '');
            echo '<br><br>';
            echo '<input type="hidden" name="instance" value="'.$conditionalinstance.'">';
            echo '<input type="hidden" name="subtype" value="'.$subtype.'">';
            echo '<input type="submit" name="saveconditional" value="'.get_string('savecondition', 'conditional').'">';
            echo '<input type="hidden" name="action" value="save">';
            break;
        case 'forum':
            $forum = get_record('forum', 'id', $conditionalinstance);
            $options['messages'] = get_string('forummessages', 'conditional');
            $options['discussions'] = get_string('forumdiscussions', 'conditional');
            $options['success'] = get_string('forumsuccess', 'conditional');
            $options['answers'] = get_string('forumanswers', 'conditional');
            if ($forum->assessed && $forum->scale) {
                $options['grade'] = get_string('forumgrade', 'conditional');
            }
            choose_from_menu($options, 'subtype', '', '');
            p($forum->name);
            echo '<br><br>';
            echo '<input type="submit" name="createconditional3" value="'.get_string('continue').'">';
            echo '<input type="hidden" name="action" value="createstep3">';
            echo '<input type="hidden" name="instance" value="'.$conditionalinstance.'">';
            break;
        case 'glossary':
            $glossary = get_record('glossary', 'id', $conditionalinstance);
            if($subtype == 'entries') {
                print_string('glossaryparticularentries', 'conditional', $glossary->name);
            } else {
                print_string('glossaryparticularcomments', 'conditional', $glossary->name);
            }
            $options = array('>'=>'>', '<'=>'<', '<='=>'<=', '>='=>'>=');
            choose_from_menu($options, 'op', '', '');
            unset($options);
            for($i=0; $i<=100 ;$i++) {
                $options[$i] = $i;
            }
            choose_from_menu($options, 'value', '', '');
            echo '<br><br>';
            echo '<input type="hidden" name="instance" value="'.$conditionalinstance.'">';
            echo '<input type="submit" name="saveconditional" value="'.get_string('savecondition', 'conditional').'">';
            echo '<input type="hidden" name="action" value="save">';
            echo '<input type="hidden" name="subtype" value="'.$subtype.'">';
            break;
        case 'quiz':
            $quiz = get_record('quiz', 'id', $conditionalinstance);
            if($subtype == 'grade') {
                print_string('quizparticulargrade', 'conditional', $quiz->name);
                $options = array('=='=>'=', '!='=>'<>', '>'=>'>', '<'=>'<', '<='=>'<=', '>='=>'>=');
                choose_from_menu($options, 'op', '', '');
                unset($options);
                for($i=0; $i<=100 ;$i++) {
                    $options[$i] = $i.' %';
                }
                choose_from_menu($options, 'value', '', '');
                echo '<br><br>';
                echo '<input type="hidden" name="instance" value="'.$conditionalinstance.'">';
                echo '<input type="submit" name="saveconditional" value="'.get_string('savecondition', 'conditional').'">';
                echo '<input type="hidden" name="action" value="save">';
                echo '<input type="hidden" name="subtype" value="'.$subtype.'">';
            } else {
                print_string('quizselectquestion', 'conditional', $quiz->name);
                $questions = get_records('quiz_question_instances', 'quiz', $quiz->id);
                foreach ($questions as $eachquestion) {
                    $question = get_record('question', 'id', $eachquestion->question);
                    $options[$eachquestion->id] = $question->name;
                }
                choose_from_menu($options, 'subtype', '', '');
                echo '<br><br>';
                echo '<input type="hidden" name="instance" value="'.$conditionalinstance.'">';
                echo '<input type="submit" name="createconditional3" value="'.get_string('continue').'">';
                echo '<input type="hidden" name="action" value="createstep3">';
            }
            break;
        case 'lesson':
            $lesson = get_record('lesson', 'id', $conditionalinstance);
            if($subtype == 'grade') {
                print_string('lessonparticulargrade', 'conditional', $lesson->name);
                $options = array('=='=>'=', '!='=>'<>', '>'=>'>', '<'=>'<', '<='=>'<=', '>='=>'>=');
                choose_from_menu($options, 'op', '', '');
                unset($options);
                for($i=0; $i<=100 ;$i++) {
                    $options[$i] = $i.' %';
                }
                choose_from_menu($options, 'value', '', '');
                echo '<br><br>';
                echo '<input type="hidden" name="instance" value="'.$conditionalinstance.'">';
                echo '<input type="submit" name="saveconditional" value="'.get_string('savecondition', 'conditional').'">';
                echo '<input type="hidden" name="action" value="save">';
                echo '<input type="hidden" name="subtype" value="'.$subtype.'">';
            } else {
                print_string('lessonselectquestion', 'conditional', $lesson->name);
                $pages = get_records('lesson_pages', 'lessonid', $lesson->id, 'id asc');
                foreach ($pages as $eachpage) {
                    $options[$eachpage->id] = $eachpage->title;
                }
                choose_from_menu($options, 'subtype', '', '');
                echo '<br><br>';
                echo '<input type="hidden" name="instance" value="'.$conditionalinstance.'">';
                echo '<input type="submit" name="createconditional3" value="'.get_string('continue').'">';
                echo '<input type="hidden" name="action" value="createstep3">';
            }
            break;
        case 'lstest':
            if($subtype == 'access') {
                print_string('lstestaccessselecttest', 'conditional');
                $lstests = get_all_instances_in_course($conditiontype, $course);
                foreach ($lstests as $eachlstest) {
                    if ($conditionalmodule->id == $module->id) {
                        $cmconditional = get_record('course_modules', 'course', $course->id, 'module', $module->id, 'instance', $eachlstest->id);
                        if ( $cmconditional->id != $cmid ) {
                            $options[$eachlstest->id] = $eachlstest->name;
                        }
                    } else {
                        $options[$eachlstest->id] = $eachlstest->name;
                    }
                }
                choose_from_menu($options, 'instance', '', '');
                echo '<br><br>';
                echo '<input type="hidden" name="op" value="!=">';
                echo '<input type="hidden" name="value" value="0">';
                echo '<input type="submit" name="saveconditional" value="'.get_string('savecondition', 'conditional').'">';
                echo '<input type="hidden" name="action" value="save">';
            } elseif ($subtype == 'style') {
                print_string('lsteststyleselecttest', 'conditional');
                $lstests = get_all_instances_in_course($conditiontype, $course);
                foreach ($lstests as $eachlstest) {
                    if ($conditionalmodule->id == $module->id) {
                        $cmconditional = get_record('course_modules', 'course', $course->id, 'module', $module->id, 'instance', $eachlstest->id);
                        if ( $cmconditional->id != $cmid ) {
                            $options[$eachlstest->id] = $eachlstest->name;
                        }
                    } else {
                        $options[$eachlstest->id] = $eachlstest->name;
                    }
                }
                choose_from_menu($options, 'instance', '', '');
                echo '<br><br>';
                echo '<input type="submit" name="createconditional3" value="'.get_string('continue').'">';
                echo '<input type="hidden" name="action" value="createstep3">';
            }
            echo '<input type="hidden" name="subtype" value="'.$subtype.'">';
            break;
        case 'subcourse':
            $subcourse = get_record('subcourse', 'id', $conditionalinstance);
            print_string('subcourseparticulargrade', 'conditional', $subcourse->name);
            $options = array('=='=>'=', '!='=>'<>', '>'=>'>', '<'=>'<', '<='=>'<=', '>='=>'>=');
            choose_from_menu($options, 'op', '', '');
            unset($options);
            for($i=0; $i<=100 ;$i++) {
                $options[$i] = $i;
            }
            choose_from_menu($options, 'value', '', '');
            echo '<br><br>';
            echo '<input type="hidden" name="instance" value="'.$conditionalinstance.'">';
            echo '<input type="submit" name="saveconditional" value="'.get_string('savecondition', 'conditional').'">';
            echo '<input type="hidden" name="action" value="save">';
            break;
    }
    echo '<input type="hidden" name="conditiontype" value="'.$conditiontype.'">';
    echo '<input type="hidden" name="conditionid" value="'.$conditionid.'">';
    echo '<input type="submit" name="cancelconditioncreation" value="'.get_string('cancel').'" onclick="document.createconditionform.action.value = \'cancel\';"/>';
    echo '</center>';
    echo '</form>';
}

function conditional_define_condition_step3($cmid, $conditiontype, $conditionalinstance=NULL, $subtype=NULL, $op=NULL, $value=NULL, $conditionid=NULL) {
    echo "<form name='createconditionform' method='POST' action='conditioncreation.php?id=".$cmid."'>";
    echo '<center>';
    switch($conditiontype) {
        case 'assignment':
            $assignment = get_record('assignment', 'id', $conditionalinstance);
            print_string('assignmentparticulargrade', 'conditional', $assignment->name);
            $options = array('=='=>'=', '!='=>'<>', '>'=>'>', '<'=>'<', '<='=>'<=', '>='=>'>=');
            choose_from_menu($options, 'op', '', '');
            unset($options);
            if($assignment->grade > 0) {
                for($i=0; $i<=$assignment->grade ;$i++) {
                    $options[$i] = $i;
                }
            } elseif($assignment->grade < 0) {
                $scale = get_record('scale', 'id', abs($assignment->grade));
                $scalevalues = explode(',', $scale->scale);
                $i=1;
                foreach ($scalevalues as $scalevalue) {
                    $options[$i] = $scalevalue;
                    $i++;
                }
            } else {
                $options = "";
            }
            choose_from_menu($options, 'value', '', '');
            break;
        case 'choice':
            $choice = get_record('choice', 'id', $conditionalinstance);
            print_string('choiceparticularansweris', 'conditional', $choice->name);
            $options = array('=='=>'=', '!='=>'<>');
            choose_from_menu($options, 'op', '', '');
            unset($options);
            $choiceoptions = get_records('choice_options', 'choiceid', $conditionalinstance);
            foreach($choiceoptions as $choiceoption){
                $options[$choiceoption->id] = $choiceoption->text;
            }
            choose_from_menu($options, 'value', '', '');
            break;
        case 'forum':
            $forum = get_record('forum', 'id', $conditionalinstance);
            if ($subtype == 'discussions') {
                print_string('forumparticulardiscussions', 'conditional', $forum->name);
                $options = array('>'=>'>', '<'=>'<', '<='=>'<=', '>='=>'>=');
            } elseif ($subtype == 'messages') {
                print_string('forumparticularmessages', 'conditional', $forum->name);
                $options = array('>'=>'>', '<'=>'<', '<='=>'<=', '>='=>'>=');
            } elseif ($subtype == 'success') {
                print_string('forumparticularsuccess', 'conditional', $forum->name);
                $options = array('>'=>'>', '<'=>'<', '<='=>'<=', '>='=>'>=');
            } elseif ($subtype == 'answers') {
                print_string('forumparticularanswers', 'conditional', $forum->name);
                $options = array('>'=>'>', '<'=>'<', '<='=>'<=', '>='=>'>=');
            } elseif ($subtype == 'grade') {
                print_string('forumparticulargrade', 'conditional', $forum->name);
                $options = array('>'=>'>', '<'=>'<', '<='=>'<=', '>='=>'>=', '=='=>'=', '!='=>'<>');
            }
            choose_from_menu($options, 'op', '>=', '');
            unset($options);
            if ($subtype == 'grade') {
                if($forum->scale > 0) {
                    for($i=0; $i<=$forum->scale ;$i++) {
                        $options[$i] = $i;
                    }
                } elseif($forum->scale < 0) {
                    $scale = get_record('scale', 'id', abs($forum->scale));
                    $scalevalues = explode(',', $scale->scale);
                    $i=1;
                    foreach ($scalevalues as $scalevalue) {
                        $options[$i] = $scalevalue;
                        $i++;
                    }
                } else {
                    $options = "";
                }
            } else {
                for($i=0; $i<=100 ;$i++) {
                    $options[$i] = $i;
                }
            }
            choose_from_menu($options, 'value', '', '');
            break;
        case 'quiz':
            $questioninstance = get_record('quiz_question_instances', 'id', $subtype);
            $question = get_record('question', 'id', $questioninstance->question);
            switch($question->qtype) {
                case 'shortanswer':
                case 'truefalse':
                case 'multichoice':
                    print_string('quizquestionanswer', 'conditional', $question->name);
                    $options = array('=='=>'=', '!='=>'<>');
                    choose_from_menu($options, 'op', '', '');
                    unset($options);
                    $answers = get_records('question_answers', 'question', $question->id);
                    foreach ($answers as $eachanswer) {
                        $options[$eachanswer->id] = $eachanswer->answer;
                    }
                    choose_from_menu($options, 'value', '', '');
                    break;
                case 'numerical':
                    print_string('quizquestionanswer', 'conditional', $question->name);
                    $options = array('=='=>'=', '!='=>'<>', '>'=>'>', '<'=>'<', '<='=>'<=', '>='=>'>=');
                    choose_from_menu($options, 'op', '', '');
                    $answer = get_record('question_answers', 'question', $question->id);
                    echo '<input type="text" name="value" value="'.$answer->answer.'">';
                    break;
                case 'match':
                    print_string('quizmatchs', 'conditional', $question->name);
                    $options = array('=='=>'=', '!='=>'<>', '>'=>'>', '<'=>'<', '<='=>'<=', '>='=>'>=');
                    choose_from_menu($options, 'op', '', '');
                    unset($options);
                    $matchsnum = count_records('question_match_sub', 'question', $question->id);
                    for($i=0; $i<=$matchsnum; $i++) {
                        $options[$i] = $i;
                    }
                    choose_from_menu($options, 'value', '', '');
                    break;
                case 'calculated':
                    print_string('quizquestionanswer', 'conditional', $question->name);
                    $options = array('=='=>'=', '!='=>'<>', '>'=>'>', '<'=>'<', '<='=>'<=', '>='=>'>=');
                    choose_from_menu($options, 'op', '', '');
                    unset($options);
                    $answers = get_records('question_answers', 'question', $question->id);
                    foreach ($answers as $eachanswer) {
                        $options[$eachanswer->id] = $eachanswer->answer;
                    }
                    choose_from_menu($options, 'value', '', '');
                    break;
                case 'randomsamatch':
                    print_string('quizmatchs', 'conditional', $question->name);
                    $options = array('=='=>'=', '!='=>'<>', '>'=>'>', '<'=>'<', '<='=>'<=', '>='=>'>=');
                    choose_from_menu($options, 'op', '', '');
                    unset($options);
                    $randomsamatch = get_record('question_randomsamatch', 'question', $question->id);
                    for($i=0; $i<=$randomsamatch->choose; $i++) {
                        $options[$i] = $i;
                    }
                    choose_from_menu($options, 'value', '', '');
                    break;
            }
            break;
        case 'lesson':
            $page = get_record('lesson_pages', 'id', $subtype);
            switch($page->qtype) {
                case 1: /// short answer
                case 2: /// true/false
                case 3: /// multichoice
                    print_string('lessonquestionanswer', 'conditional', $page->title);
                    $options = array('=='=>'=', '!='=>'<>');
                    choose_from_menu($options, 'op', '', '');
                    unset($options);
                    $answers = get_records('lesson_answers', 'pageid', $page->id, 'id asc');
                    foreach ($answers as $eachanswer) {
                        $options[$eachanswer->id] = $eachanswer->answer;
                    }
                    choose_from_menu($options, 'value', '', '');
                    break;
                case 8: /// numerical
                    print_string('lessonquestionanswer', 'conditional', $page->title);
                    $options = array('=='=>'=', '!='=>'<>', '>'=>'>', '<'=>'<', '<='=>'<=', '>='=>'>=');
                    choose_from_menu($options, 'op', '', '');
                    $answer = get_record('lesson_answers', 'pageid', $page->id);
                    echo '<input type="text" name="value" value="'.$answer->answer.'">';
                    break;
                case 5: /// match
                    print_string('lessonmatchs', 'conditional', $page->title);
                    $options = array('=='=>'=', '!='=>'<>', '>'=>'>', '<'=>'<', '<='=>'<=', '>='=>'>=');
                    choose_from_menu($options, 'op', '', '');
                    unset($options);
                    $matchsnum = (count_records('lesson_answers', 'pageid', $page->id) - 2);
                    for($i=0; $i<=$matchsnum; $i++) {
                        $options[$i] = $i;
                    }
                    choose_from_menu($options, 'value', '', '');
                    break;
                case 10: /// essay
                    print_string('lessonessaygrade', 'conditional', $page->title);
                    $options = array('=='=>'=', '!='=>'<>', '>'=>'>', '<'=>'<', '<='=>'<=', '>='=>'>=');
                    choose_from_menu($options, 'op', '', '');
                    unset($options);
                    $answer = get_record('lesson_answers', 'pageid', $page->id);
                    for($i=0; $i<=$answer->score; $i++) {
                        $options[$i] = $i;
                    }
                    choose_from_menu($options, 'value', '', '');
                    break;
            }
            break;
        case 'lstest':
            $lstest = get_record('lstest', 'id', $conditionalinstance);
            print_string('lstestparticularstyle', 'conditional', $lstest->name);
            $options = array('=='=>'=', '!='=>'<>');
            choose_from_menu($options, 'op', '', '');
            unset($options);
            $styleoptions = get_records('lstest_styles', 'testsid', $lstest->testsid, 'id ASC');
            foreach($styleoptions as $styleoption){
                $options[$styleoption->id] = $styleoption->name;
            }
            choose_from_menu($options, 'value', '', '');
            break;
    }
    echo '<br><br>';
    echo '<input type="submit" name="saveconditional" value="'.get_string('savecondition', 'conditional').'">';
    echo '<input type="hidden" name="action" value="save">';
    echo '<input type="hidden" name="conditiontype" value="'.$conditiontype.'">';
    echo '<input type="hidden" name="subtype" value="'.$subtype.'">';
    echo '<input type="hidden" name="instance" value="'.$conditionalinstance.'">';
    echo '<input type="hidden" name="conditionid" value="'.$conditionid.'">';
    echo '<input type="submit" name="cancelconditioncreation" value="'.get_string('cancel').'" onclick="document.createconditionform.action.value = \'cancel\';"/>';
    echo '</center>';
    echo '</form>';
}

?>
