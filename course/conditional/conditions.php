<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once("../../config.php");
require_once("$CFG->dirroot/course/lib.php");
require_once("conditionslib.php");

require_login();

$id = optional_param('id', 0, PARAM_INT);
$conditiontype = optional_param('conditiontype', NULL);
$deleteconditional = optional_param('deleteconditional', NULL);
$addconector = optional_param('addconector', NULL);
$addid = optional_param('addid', NULL);
$details = optional_param('details', NULL);
$subtype = optional_param('subtype', NULL);
$conditionalinstance = optional_param('instance', NULL, PARAM_INT);
$isused = optional_param('isused', NULL);
$conditionid = optional_param('conditionid', NULL, PARAM_INT);
$action = optional_param('action', NULL);
// If noscript we must expect that params
$activateconditional = optional_param('activateconditional', NULL);
$deactivateconditional = optional_param('deactivateconditional', NULL);
$agrupate = optional_param('agrupate', NULL);
$agrupateandreturn = optional_param('agrupateandreturn', NULL);


if (! $cm = get_record("course_modules", "id", $id)) {
    print_error('invalidcoursemodule');
}

if (! $course = get_record("course", "id", $cm->course)) {
    print_error('invalidcourse');
}

require_login($course);
$context = get_context_instance(CONTEXT_MODULE, $cm->id);
require_capability('moodle/local:createconditionals', $context);

if (! $module = get_record("modules", "id", $cm->module)) {
    print_error('cmunknown');
}

if (! $activity = get_record($module->name, "id", $cm->instance)) {
    print_error('cmunknown');
}

if(isset($deleteconditional)) {
    conditional_delete_condition($cm->id, $deleteconditional, $isused);
}

if($action == 'activate' || $activateconditional) {
    conditional_activate($cm->id);
}

if($action == 'deactivate' || $deactivateconditional) {
    conditional_deactivate($cm->id);
}

if( ($action == 'agrupate' || $agrupate ) and isset($details)) {
    conditional_agrupate($course->id, $cm->id, $details, $agrupateconditionalerror, false);
} elseif(($action == 'agrupateandreturn'  || $agrupateandreturn ) and isset($details)) {
    conditional_agrupate($course->id, $cm->id, $details, $agrupateconditionalerror, true);
}

// Print the tabs
$overridableroles = get_overridable_roles($context);
$assignableroles  = get_assignable_roles($context);
$currenttab = 'locks';
include_once($CFG->dirroot.'/'.$CFG->admin.'/roles/tabs.php');

// Print the conditions table zone
$members = array();
if ($conditional = get_record('conditional', 'moduleid', $cm->id)) {
    $members = get_records('conditional_member', 'conditionalid', $conditional->id, 'tableid asc');
}

if (isset($addconector)) {
    $details .= ' '.$addconector;
} elseif (isset($addid)) {
    $details = str_replace('_', ' ', $details);
    $details .= ' '.$addid;
} elseif ($conditional && !isset($agrupateconditionalerror) && !isset($deactivateconditional) && !isset($activateconditional)) {
    $details = $conditional->details;
}

print_heading_with_help(get_string('createdconditions', 'conditional', $activity->name), 'conditionaltable', 'conditional');
if($members) {
    $table = new stdClass();
    $table->head = array(get_string('used', 'conditional'), get_string('condition', 'conditional'), get_string('id', 'conditional'), get_string('edit', 'conditional'), get_string('delete', 'conditional'));
    $table->align = array('center', 'left', 'center', 'center', 'center');
    $table->data = array();

    $conditionaldetail = $conditional->details;
    $membersdesc = get_records('conditional_member', 'conditionalid', $conditional->id, 'id desc');
    $imgused = '<img src="'.$CFG->pixpath.'/i/tick_green_big.gif">';
    $imgnotused = '<img src="'.$CFG->pixpath.'/i/cross_red_big.gif">';
    $pixdelete = $CFG->pixpath.'/t/delete.gif';
    $strdelete = get_string('delete', 'conditional');
    $pixedit = $CFG->pixpath.'/t/edit.gif';
    $stredit = get_string('edit', 'conditional');
    $used = array();
    foreach ($membersdesc as $memberdesc) {
        if (strstr($conditionaldetail, "$memberdesc->tableid")) {
            $conditionaldetail = str_replace($memberdesc->tableid, '', $conditionaldetail);
            $used[$memberdesc->tableid] = $imgused;
        } else {
            $used[$memberdesc->tableid] = $imgnotused;
        }
    }

    foreach ($members as $member) {
        unset($condition);
        switch($member->type) {
            case 'assignment':
                $conditionalinstance = get_record($member->type, 'id', $member->instance);
                if ($member->subtype == 'submit') {
                    if ($member->op == '>') {
                        $text = get_string('assignmentparticularhassubmit', 'conditional', $conditionalinstance->name);
                    } else {
                        $text = get_string('assignmentparticularhasntsubmit', 'conditional', $conditionalinstance->name);
                    }
                } elseif ($member->subtype == 'grade') {
                    $condition->instance = $conditionalinstance->name;
                    $condition->op = operator_to_string($member->op);
                    $text = get_string('assignmentstudentgrade', 'conditional', $condition);
                    if ($conditionalinstance->grade > 0) {
                        $text .= $member->value;
                    } elseif ($conditionalinstance->grade < 0) {
                        $scale = get_record('scale', 'id', abs($conditionalinstance->grade));
                        $scalevalues = explode(',', $scale->scale);
                        $text .= $scalevalues[$member->value - 1];
                    }
                }
                break;
            case 'book':
                $conditionalinstance = get_record($member->type, 'id', $member->instance);
                $condition = $conditionalinstance->name;
                if ($member->op == '>') {
                    $text = get_string('bookparticipanthasaccessdisplay', 'conditional', $condition);
                } else {
                    $text = get_string('bookparticipanthasntaccessdisplay', 'conditional', $condition);
                }
                break;
            case 'chat':
                $conditionalinstance = get_record($member->type, 'id', $member->instance);
                if($member->subtype == 'say') {
                    $text = get_string('chatparticularhasparticipated', 'conditional', $conditionalinstance->name);
                } elseif($member->subtype == 'notsay') {
                    $text = get_string('chatparticularhasntparticipated', 'conditional', $conditionalinstance->name);
                }
                break;
            case 'choice':
                $conditionalinstance = get_record($member->type, 'id', $member->instance);
                $option = get_record('choice_options', 'id', $member->value);
                $condition->instance = $conditionalinstance->name;
                if($member->subtype == 'anyanswer') {
                    $text = get_string('choiceanyanswerdisplay', 'conditional', $condition);
                } elseif ($member->subtype == 'particularanswer') {
                    $condition->op = operator_to_string($member->op);
                    $condition->text = $option->text;
                    $text = get_string('choiceparticularanswerdisplay', 'conditional', $condition);
                }
                break;
            case 'conditionalbook':
                $conditionalinstance = get_record($member->type, 'id', $member->instance);
                $condition = $conditionalinstance->name;
                if ($member->op == '>') {
                    $text = get_string('conditionalbookparticipanthasaccessdisplay', 'conditional', $condition);
                } else {
                    $text = get_string('conditionalbookparticipanthasntaccessdisplay', 'conditional', $condition);
                }
                break;
            case 'dialogue':
                $conditionalinstance = get_record($member->type, 'id', $member->instance);
                $condition->instance = $conditionalinstance->name;
                $condition->op = operator_to_string($member->op);
                $condition->value = $member->value;
                $text = get_string('dialogueparticipantconversations', 'conditional', $condition);
                break;
            case 'forum':
                $conditionalinstance = get_record($member->type, 'id', $member->instance);
                $condition->instance = $conditionalinstance->name;
                $condition->op = operator_to_string($member->op);
                $condition->value = $member->value;
                if ($member->subtype == 'discussions') {
                    $text = get_string('forumconditionparticulardiscussions', 'conditional', $condition);
                } elseif($member->subtype == 'messages') {
                    $text = get_string('forumconditionparticularmessages', 'conditional', $condition);
                } elseif($member->subtype == 'success') {
                    $text = get_string('forumconditionparticularsuccess', 'conditional', $condition);
                } elseif($member->subtype == 'answers') {
                    $text = get_string('forumconditionparticularanswers', 'conditional', $condition);
                } elseif($member->subtype == 'grade') {
                    $text = get_string('forumconditionparticulargrade', 'conditional', $condition);
                    if ($conditionalinstance->scale > 0) {
                        $text .= $member->value;
                    } elseif ($conditionalinstance->scale < 0) {
                        $scale = get_record('scale', 'id', abs($conditionalinstance->scale));
                        $scalevalues = explode(',', $scale->scale);
                        $text .= $scalevalues[$member->value - 1];
                    }
                }
                break;
            case 'glossary':
                $conditionalinstance = get_record($member->type, 'id', $member->instance);
                $condition->instance = $conditionalinstance->name;
                $condition->op = operator_to_string($member->op);
                $condition->value = $member->value;
                if($member->subtype == 'entries') {
                    $text = get_string('glossaryconditionparticularentries', 'conditional', $condition);
                } elseif($member->subtype == 'comments') {
                    $text = get_string('glossaryconditionparticularcomments', 'conditional', $condition);
                }
                break;
            case 'lesson':
                $conditionalinstance = get_record($member->type, 'id', $member->instance);
                if($member->subtype == 'grade') {
                    $condition->instance = $conditionalinstance->name;
                    $condition->op = operator_to_string($member->op);
                    $condition->value = $member->value;
                    $text = get_string('lessonconditiongrade', 'conditional', $condition);
                } else {
                    $page = get_record('lesson_pages', 'id', $member->subtype);

                    $condition->question = $page->title;
                    $condition->instance = $conditionalinstance->name;
                    $condition->op = operator_to_string($member->op);

                    switch($page->qtype) {
                        case 1: /// short answer
                        case 2: /// true/false
                        case 3: /// multichoice
                            $answer = get_record('lesson_answers', 'id', $member->value);
                            $condition->answer = $answer->answer;
                            $text = get_string('lessonquestionshort', 'conditional', $condition);
                            break;
                        case 8: /// numerical
                            $condition->value = $member->value;
                            $text = get_string('lessonquestionnumerical', 'conditional', $condition);
                            break;
                        case 5: /// match
                            $condition->value = $member->value;
                            $text = get_string('lessonquestionmatch', 'conditional', $condition);
                            break;
                        case 10: /// essay
                            $condition->value = $member->value;
                            $text = get_string('lessonquestionessay', 'conditional', $condition);
                            break;
                    }
                }
                break;
            case 'quiz':
                $conditionalinstance = get_record($member->type, 'id', $member->instance);
                if($member->subtype == 'grade') {
                    $condition->instance = $conditionalinstance->name;
                    $condition->op = operator_to_string($member->op);
                    $condition->value = $member->value;
                    $text = get_string('quizconditiongrade', 'conditional', $condition);
                } else {
                    $questioninstance = get_record('quiz_question_instances', 'id', $member->subtype);
                    $question = get_record('question', 'id', $questioninstance->question);

                    $condition->question = $question->name;
                    $condition->instance = $conditionalinstance->name;
                    $condition->op = operator_to_string($member->op);

                    switch($question->qtype) {
                        case 'shortanswer':
                        case 'truefalse':
                        case 'multichoice':
                        case 'calculated':
                            $answer = get_record('question_answers', 'id', $member->value);
                            $condition->answer = $answer->answer;
                            $text = get_string('quizquestionshort', 'conditional', $condition);
                            break;
                        case 'numerical':
                            $condition->value = $member->value;
                            $text = get_string('quizquestionnumerical', 'conditional', $condition);
                            break;
                        case 'match':
                            $condition->value = $member->value;
                            $text = get_string('quizquestionmatch', 'conditional', $condition);
                            break;
                        case 'randomsamatch':
                            $condition->value = $member->value;
                            $text = get_string('quizquestionrandom', 'conditional', $condition);
                            break;
                    }
                }
                break;
            case 'resource':
                $conditionalinstance = get_record($member->type, 'id', $member->instance);
                $condition = $conditionalinstance->name;
                if ($member->op == '>') {
                    $text = get_string('resourcestudenthasaccessdisplay', 'conditional', $condition);
                } else {
                    $text = get_string('resourcestudenthasntaccessdisplay', 'conditional', $condition);
                }
                break;
            case 'questionnaire':
                $conditionalinstance = get_record($member->type, 'id', $member->instance);
                $condition = $conditionalinstance->name;
                if ($member->op == '>') {
                    $text = get_string('questionnairestudenthasanswerdisplay', 'conditional', $condition);
                } else {
                    $text = get_string('questionnairestudenthasntanswerdisplay', 'conditional', $condition);
                }
                break;
            case 'lstest':
                $conditionalinstance = get_record($member->type, 'id', $member->instance);
                if($member->subtype == 'access') {
                    $condition = $conditionalinstance->name;
                    $text = get_string('lsteststudenthasaccessdisplay', 'conditional', $condition);
                } elseif($member->subtype == 'style') {
                    $style = get_record('lstest_styles', 'id', $member->value);
                    $condition->instance = $conditionalinstance->name;
                    $condition->op = operator_to_string($member->op);
                    $condition->value = $style->name;
                    $text = get_string('lsteststudentstyledisplay', 'conditional', $condition);
                }
                break;
            case 'wiki':
                $conditionalinstance = get_record($member->type, 'id', $member->instance);
                $condition->instance = $conditionalinstance->name;
                $condition->op = operator_to_string($member->op);
                $condition->value = $member->value;
                $text = get_string('wikiparticular', 'conditional', $condition);
                break;
            case 'workshop':
                $conditionalinstance = get_record($member->type, 'id', $member->instance);
                $condition->instance = $conditionalinstance->name;
                $condition->op = operator_to_string($member->op);
                $condition->value = $member->value;
                $text = get_string('workshopparticular', 'conditional', $condition);
                break;
            case 'role':
                $condition->op = operator_to_string($member->op);
                $condition->value = $member->value;
                $text = get_string('roleparticular', 'conditional', $condition);
                break;
            case 'lang':
                $condition->op = operator_to_string($member->op);
                $condition->value = $member->value;
                $text = get_string('studentlangparticular', 'conditional', $condition);
                break;
            case 'country':
                $condition->op = operator_to_string($member->op);
                $condition->value = $member->value;
                $text = get_string('studentcountryparticular', 'conditional', $condition);
                break;
            case 'picture':
                if ($member->op == '>') {
                    $text = get_string('picturehaschanged', 'conditional');
                } elseif ($member->op == '==') {
                    $text = get_string('picturehasnotchanged', 'conditional');
                }
                break;
            case 'enrolment':
                $condition->op = operator_to_string($member->op);
                $condition->days = $member->value;
                $text = get_string('enrolmenttimedifferencedisplay', 'conditional', $condition);
                break;
            case 'startdate':
                $condition->op = operator_to_string($member->op);
                $condition->days = $member->value;
                $text = get_string('starttimedifferencedisplay', 'conditional', $condition);
                break;
            case 'date':
                $condition->date = userdate($member->instance);
                $condition->op = operator_to_string($member->op);
                $condition->days = $member->value;
                $text = get_string('datetimedifferencedisplay', 'conditional', $condition);
                break;
            case 'message':
                $condition->op = operator_to_string($member->op);
                $condition->value = $member->value;
                $text = get_string('messageconditiondisplay', 'conditional', $condition);
                break;
            case 'subcourse':
                $conditionalinstance = get_record($member->type, 'id', $member->instance);
                $condition->instance = $conditionalinstance->name;
                $condition->op = operator_to_string($member->op);
                $condition->value = $member->value;
                $text = get_string('subcourseconditiongrade', 'conditional', $condition);
                break;
            default:
                $text = '';
                break;
        }

        if (strstr($conditional->details, "$member->tableid")) {
            $detailsprobe = $conditional->details;
            $tableidlength = strlen("$member->tableid");
            $isused = false;
            while ( strstr($detailsprobe, "$member->tableid") and !$isused) {
                $position = strpos($detailsprobe, "$member->tableid");
                $isused = true;
                if ( ($position > 0) and strstr('0123456789', substr($detailsprobe, $position - 1, 1))) {
                    $isused = false;
                }
                if ( (($position + $tableidlength) < strlen($detailsprobe) ) and strstr('0123456789', substr($detailsprobe, $position + $tableidlength, 1))) {
                    $isused = false;
                }
                if (!$detailsprobe = substr($detailsprobe, $position + $tableidlength + 1)) {
                    $detailsprobe = '';
                }
            }
        } else {
            $isused = false;
        }

        $deleteurl = 'conditions.php?id='.$cm->id.'&amp;deleteconditional='.$member->id.'&amp;isused='.$isused.'#labelconditional';

        $delete = "<noscript><a href='$deleteurl' title='$strdelete'><img src='$pixdelete' align='absmiddle' border=0></a></noscript><script><!--
            document.write(\"<a href='#labelconditional'  title='$strdelete' onClick=checkdeletecondition('$isused','$deleteurl');><img src='$pixdelete' align='absmiddle' border=0></a>\");
            --></script>";


        $edit = '<a href=conditioncreation.php?id='.$cm->id.'&amp;conditionid='.$member->id." title=\"$stredit\">"."<img src=\"$pixedit\"></a>";

        $identifier = "<noscript><input type='submit' name='addconector' value='$member->tableid'></noscript><script><!--
            document.write(\"<input type=button value='$member->tableid' title='".get_string('addcondition', 'conditional', $member->tableid)."' onClick=addconector('$member->tableid');>\");
            --></script>";

        array_push($table->data, array($used[$member->tableid], $text, $identifier, $edit, $delete));
    }

    echo "<noscript><form name='agrupateconditionsform' method='POST' action='conditions.php?id=".$cm->id."#labelagrupateconditional'></noscript>";
    print_table($table);
    echo "<noscript><input type='hidden' name='details' value='$details'></form></noscript>";

?>

    <script>
    <!-- // BEGIN

        function checkdeletecondition(isinagregation, url) {
            if (isinagregation) {
                if (confirm("<?php print_string('deleteusedcondition', 'conditional') ?>")) {
                    window.location = url;
                }
            } else {
                window.location = url;
            }
        }

    // END -->
    </script>

<?php

} else {
    echo "<center>";
    print_string('nocreatedconditions', 'conditional');
    echo "</center>";
}

echo "<br>";
echo "<center>";
echo "<form name='createconditionform' method='POST' action='conditioncreation.php?id=".$cm->id."'>";
echo '<input type="submit" name="createcondition" value="'.get_string('createcondition', 'conditional').'">';
helpbutton('conditionalcreation', get_string('createcondition', 'conditional'), 'conditional');
echo "</form>";
echo "</center>";

// Agrupate conditions zone
if ($members) {

    // Print heading and advisory text
    echo '<br><br>';
    echo '<a name="labelagrupateconditional"></a>';
    print_heading_with_help(get_string('agrupateconditions', 'conditional'), 'conditionalgrouping', 'conditional');
    echo '<center>';
    echo "<form name='agrupateconditionsform' method='POST' action='conditions.php?id=".$cm->id."#labelagrupateconditional'>";
    echo '<input type="hidden" name="action">';
    print_string('agregateconditionswarning', 'conditional');
    echo '<br><br>';

    // Print conectors buttons
?>
    <script>
    <!-- // BEGIN

        function addconector(conector) {
            document.agrupateconditionsform.details.value = document.agrupateconditionsform.details.value + " " + conector;
        }

        document.write("<input type=button value='(' onClick=addconector(\"(\");>");
        document.write("<input type=button value=')' onClick=addconector(\")\");>");
        document.write("<input type=button value='not' onClick=addconector(\"not\");>");
        document.write("<input type=button value='and' onClick=addconector(\"and\");>");
        document.write("<input type=button value='or' onClick=addconector(\"or\");>");

    // END -->
    </script>

    <noscript>
        <input type="submit" name="addconector" value="(">
        <input type="submit" name="addconector" value=")">
        <input type="submit" name="addconector" value="not">
        <input type="submit" name="addconector" value="and">
        <input type="submit" name="addconector" value="or">
    </noscript>

<?php
    // Print the rest of the agrupate conditions zone
    echo '<br><br>';
    print_string('conditionscombination', 'conditional');
    echo '<input type="text" name="details" value="'.$details.'" size="60">';

    if ($conditional->inuse) {
        echo '<input type="submit" name="deactivateconditional" value="'.get_string('deactivateconditional', 'conditional').'" onclick="document.agrupateconditionsform.action.value = \'deactivate\';">';
    } else {
        echo '<input type="submit" name="activateconditional" value="'.get_string('activateconditional', 'conditional').'" onclick="document.agrupateconditionsform.action.value = \'activate\';">';
        echo '<br><br>';
        echo '<FONT color=red>'.get_string('conditionaldisabled', 'conditional').'</FONT>';
    }

    if( ($action == 'agrupate' || $action == 'agrupateandreturn' || $agrupate || $agrupateandreturn) and isset($agrupateconditionalerror)) {
        echo '<br><br>';
        echo '<FONT color=red>'.get_string('invalidsyntax', 'conditional').'</FONT>';
    }

?>
    <br><br>
    <input type="submit" name="agrupate" value="<?php print_string('savechanges') ?>" onclick="document.agrupateconditionsform.action.value = 'agrupate';">
    <input type="submit" name="agrupateandreturn" value="<?php print_string('savechangesandreturntocourse') ?>" onclick="document.agrupateconditionsform.action.value = 'agrupateandreturn';">

<?php

    echo '</form>';
    echo '</center>';

}
print_footer($course);

function operator_to_string($operator) {
    switch($operator) {
        case '==':
            $text = get_string('equal', 'conditional');
            break;
        case '!=':
            $text = get_string('distinct', 'conditional');
            break;
        case '>':
            $text = get_string('greater', 'conditional');
            break;
        case '<':
            $text = get_string('less', 'conditional');
            break;
        case '>=':
            $text = get_string('greaterequal', 'conditional');
            break;
        case '<=':
            $text = get_string('lessequal', 'conditional');
            break;
        default:
            $text = '';
            break;
    }
    return $text;
}

?>